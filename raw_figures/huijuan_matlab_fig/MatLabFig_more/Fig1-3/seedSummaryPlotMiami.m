data1 = importdata('/usr/local/dac/workspace/socnet/output/MiamiKDD/seed5Top3WDegreeSummary');
data2 = importdata('/usr/local/dac/workspace/socnet/output/MiamiKDD/seed5Top5WDegreeSummary');
data3 = importdata('/usr/local/dac/workspace/socnet/output/MiamiKDD/seed5Top10WDegreeSummary');
data4 = importdata('/usr/local/dac/workspace/socnet/output/MiamiKDD/seed5Top20WDegreeSummary');

input_name= '/usr/local/dac/workspace/socnet/output/MiamiKDD/'

%data= importdata(input_name);
data(:,1) = data1(:,2)
data(:,2) = data1(:,3)
data(:,3) = data2(:,3)
data(:,4) = data3(:,3)
data(:,5) = data4(:,3)

scrsz = get(0,'ScreenSize');

colors = [0,0,1; % blue
    1 0 0;  % red
    0 1 0; % green
    1 0 1; % magenta
    0 1 1;  % cyan
    0 0 0; % black
    1 1 0;  % yellow
    0.6377    0.9577    0.2407; % light green
    0.8754    0.5181    0.9436; % purple
    0.6951    0.0680    0.2548; % dark pink
    %0.3686    0.5098    0.7176; % light green
    0.1412    0.5098    0.5255; % purple
    0.0588    0.1529    0.2941; % dark pink
    ];

Markers = 'ox+*sdv^<>ph';

percent=[1,5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90];
percentNo=0;
scrsz = get(0,'ScreenSize');

h1=figure('Position',[1 1 scrsz(3) scrsz(4)]);
for i=2:5
xlim([0 90]);
ylim([0 5]);
p=plot(data(:,1), data(:,i),'+b-','LineWidth',10);
set(p,'Color',colors(i,:),'marker',Markers(i),'MarkerSize',49);
hold on
end

h_legend= legend('Top-3 Weight Degree ', 'Top-5 Weight Degree ', 'Top-10 Weight Degree ','Top-20 Weight Degree ');
set(h_legend, 'FontSize',49, 'Position', [0.3, 0.51, 0.65, 0.53]);
set(findobj('type','axes'), 'FontSize',69);
title('Shift Days with Sensor Size (SEIR:4.2E-5)');
xlabel('Random and Sensor Group Size (percent)');
ylabel('Shift Days (day)');
hold off;
set(gcf, 'PaperUnits', 'centimeters');
%set(gcf, 'PaperType', 'A4');
set(gcf, 'PaperPosition', [1 1 58 58]);
set(gcf, 'PaperSize', [59 59]);
fig1name=[input_name 'miamiseed5TopKWDegreePercent.pdf']
%print(h1,'-depsc',fig2name);
print(h1,'-dpdf',fig1name)



h2=figure('Position',[1 1 scrsz(3) scrsz(4)]);
xlim([0 21]);
ylim([0 5]);
p=plot([3,5,10 20]', [mean(data(:,2)),mean(data(:,3)),mean(data(:,4)),mean(data(:,5))]','+b-','LineWidth',10);
set(p,'Color',colors(i,:),'marker',Markers(i),'MarkerSize',49);
hold on

h_legend= legend('Shifit days ' );
set(h_legend, 'FontSize',52, 'Position', [0.46, 0.51, 0.48, 0.53]);
set(findobj('type','axes'), 'FontSize',69);
title('Shift Days with Top-K Weight Degree (SEIR:4.2E-5)');
xlabel('K Value of Top-K Weight Degree of Random Group');
ylabel('Shift Days (day)');
hold off;
set(gcf, 'PaperUnits', 'centimeters');
%set(gcf, 'PaperType', 'A4');
set(gcf, 'PaperPosition', [1 1 58 58]);
set(gcf, 'PaperSize', [59 59]);
fig2name=[input_name 'miamiShiftTopK.pdf']
%print(h1,'-depsc',fig2name);
print(h2,'-dpdf',fig2name)

