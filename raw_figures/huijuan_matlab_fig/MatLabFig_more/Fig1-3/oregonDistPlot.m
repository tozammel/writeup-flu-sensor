filename = 'oregonDist.data';
delimiterIn = ',';
A = importdata(filename,delimiterIn);

figure(1)
hist(A,20)
%h_legend= legend('True Friend','Fit Friend','True Random','Fit Random');
%set(h_legend, 'FontSize',12)
set(findobj('type','axes'), 'FontSize',12)
xlabel('Lead Time (day)');
ylabel('Frequency');
