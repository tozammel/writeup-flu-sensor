input_dir= '/usr/local/dac/workspace/socnet/output/OregonKDD/'
input_name= [input_dir '20130221/seed5Top3DegreeSummary']

data = importdata(input_name);

colors = [0,0,1; % blue
    1 0 0;  % red
    0 1 0; % green
    1 0 1; % magenta
    0 1 1;  % cyan
    0 0 0; % black
    1 1 0;  % yellow
    0.6377    0.9577    0.2407; % light green
    0.8754    0.5181    0.9436; % purple
    0.6951    0.0680    0.2548; % dark pink
    %0.3686    0.5098    0.7176; % light green
    0.1412    0.5098    0.5255; % purple
    0.0588    0.1529    0.2941; % dark pink
    ];

Markers = 'ox+*sdv^<>ph';

percent=[1,5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90];
percentNo=0;
scrsz = get(0,'ScreenSize');

h1= figure('Position',[1 1 scrsz(3) scrsz(4)]);
for i=1:12;
percentNo = percent(i);
X= [data(data(:,2)==percentNo,1), data(data(:,2)==percentNo,3)];
xlim([0 1]);
ylim([-1 10]);
p= plot(X(:,1), X(:,2),'+b-', 'LineWidth', 2);
set(p,'Color',colors(i,:),'marker',Markers(i),'MarkerSize',10);
hold on;
end;
h_legend= legend('Percent 1', 'Percent 5','Percent 10','Percent 20','Percent 30','Percent 40','Percent 50','Percent 60','Percent 70','Percent 80','Percent 90');
set(h_legend, 'FontSize',29);
set(findobj('type','axes'), 'FontSize',39);
title('Shift Days with SI Transmission Rate');
xlabel('SI Transmission Rate');
ylabel('Shift Days (day)');
hold off
set(gcf, 'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A4');
set(gcf, 'PaperPosition', [1 1 30 30]);
set(gcf, 'PaperSize', [32 32]);
fig1name=[input_dir 'TransRateShiftDays.pdf']
print(h1,'-dpdf',fig1name);

%%% plot for SI rate
siRate=[0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9];
siRateNo=0;


h2=figure('Position',[1 1 scrsz(3) scrsz(4)]);
for i=1:10;
siRateNo = siRate(i);
X= [data(data(:,1)==siRateNo,2), data(data(:,1)==siRateNo,3)];
xlim([0 90]);
ylim([-1 10]);
p=plot(X(:,1), X(:,2),'+b-','LineWidth',2);
set(p,'Color',colors(i,:),'marker',Markers(i),'MarkerSize',10);
hold on
end
h_legend= legend('p=0.05', 'rate=0.1','rate=0.2','rate=0.3','rate=0.4','rate=0.5','rate=0.6','rate=0.7','rate=0.8','rate=0.9');
set(h_legend, 'FontSize',29);
set(findobj('type','axes'), 'FontSize',39);
title('Shift Days with Random/Sensor Group Size Rate');
xlabel('Random and Sensor Group Rate (percent)');
ylabel('Shift Days (day)');
hold off;
set(gcf, 'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A4');
set(gcf, 'PaperPosition', [1 1 30 30]);
set(gcf, 'PaperSize', [32 32]);
fig2name=[input_dir 'PercentShiftDays.pdf']
print(h2,'-dpdf',fig2name);

% h3=figure('Position',[1 1 scrsz(3) scrsz(4)]);
% for i=1:10;
% siRateNo = siRate(i);
% X= [data(data(:,1)==siRateNo,2), data(data(:,1)==siRateNo,3)];
% xlim([0 90]);
% ylim([-1 10]);
% p=plot(X(:,1), X(:,2),'+b-','LineWidth',2);
% set(p,'Color',colors(i,:),'marker',Markers(i),'MarkerSize',10);
% hold on
% end
% h_legend= legend('rate=0.05', 'rate=0.1','rate=0.2','rate=0.3','rate=0.4','rate=0.5','rate=0.6','rate=0.7','rate=0.8','rate=0.9');
% set(h_legend, 'FontSize',29);
% set(findobj('type','axes'), 'FontSize',39);
% title('Shift Days with Random/Sensor Group Size Rate');
% xlabel('Random and Sensor Group Rate (percent)');
% ylabel('Shift Days (day)');
% hold off;
% set(gcf, 'PaperUnits', 'centimeters');
% set(gcf, 'PaperType', 'A4');
% set(gcf, 'PaperPosition', [1 1 30 30]);
% set(gcf, 'PaperSize', [32 32]);
% fig2name=[input_dir 'TransRateShiftDays.pdf']
% print(h2,'-dpdf',fig2name);


