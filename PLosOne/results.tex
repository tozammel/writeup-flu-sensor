\section*{Results}
Our experimental investigations focus on addressing the following questions:
\begin{enumerate*}
	\item How well do the algorithms perform for the MAIT problem (the infection
        time problem)? (see Section~\ref{infection-time})
	\item For a given sensor network size, how well do the algorithms perform in
        terms of the PLTM problem (the peak lead time problem)? (see
        Section~\ref{lead-time}) 
	\item How large should our sensor set size be? (see Section~\ref{sensor-size})
	\item How does lead time vary as a function of disease parameters? (see
        Section~\ref{disease-param})
  \item How many days are necessary to observe a stable lead time? (see
        Section~\ref{stability})
	\item What is the predictive power of the sensor set in estimating the
        epidemic curve over the full population? (see Section~\ref{predict-epicurve})
  \item Is it possible to employ surrogates for sensors? (see Section~\ref{surrogates})
\end{enumerate*}
For all the experiments in this section, we set $\epsilon = 0.8$ (see the
definitions of the PLTM and MAIT problems) and flu transmission rate to be
$4.2 \times 10^{-5}$ for SEIR disease model, and all the results were obtained
by averaging across $1,000$ independent runs unless specified.

\subsection*{Datasets}
\label{sec:dataset}
Table~\ref{tab:dataset} shows some basic network statistics of the datasets we
used in our experiments. The Oregon AS (Autonomous System) router graph is an
AS-level connectivity network inferred from Oregon
route-views~\cite{oregondata}. Although this dataset does not relate to
epidemiological modeling, we use it primarily as a testbed to understand how
(and if) graph topology affects our results due to the relatively small size and
neat graph structure; the rest of the datasets are generated with specific aim
at modeling epidemics in human populations.  These datasets are synthetic but
realistic social contact networks for seven large cities in the United States.
Here, we briefly describe the major steps to generate these synthetic datasets
(see~\cite{barrett:wsc09,nature} for details): (i) a synthetic urban population
model is constructed by integrating a variety of public (e.g., US Census) and
commercial data (Dunn \& BradStreet), which is statistically equivalent to the
real population; (ii) activity sequences are constructed for each household and
each person by matching activity surveys. The activity data also specify the
type of each activity and the duration of performing it; (iii) activity
locations are assigned for each person, using land use data and activity choice
models; (iv) individuals are routed through the road network, which gives a
social contact network based on location co-occurrences.

\begin{table}[!t]
	\centering
	\caption{\textbf{Statistics of datasets used in the experiments.}}
	\label{tab:dataset}
	\begin{tabular}{|l|c|c|c|}
	  \hline
	  Dataset  & Nodes     & Avg.\ deg &  Max deg\\
	  \hline
	  \oregon  & 10,670    & 4.12     &  2,312\\
	  \hline
	  \miami   & 2,092,147 & 50.38     &  425 \\
	  \hline
	  %\seattle & 3,207,037 & 55.35     &  456\\
	  %\hline
	  \boston & 4,149,279 & 108.32 & 437 \\
	   \hline
	  \dallas & 5,098,598 & 113.10  & 477 \\
	   \hline
	  \chicago & 9,047,574 & 118.83  & 507 \\
	   \hline
	  \losangeles & 16,244,426 & 113.08 & 463 \\
	   \hline
	  \newyork & 20,618,488 & 93.14 & 464 \\
	  \hline
	\end{tabular}
\end{table}

\subsection*{Algorithms}
\label{sec:algo}
We evaluated our two proposed approaches, transmission tree based heuristic and
dominator tree based heuristic, on the datasets mentioned in
Table.~\ref{tab:dataset}. For comparison, we also implemented two strategies as
baseline methods: (i) \textbf{Top-K high degree sensors} heuristic used
in~\cite{christakis:10:sensor} in which a set $P \subseteq V$ is first sampled
and for each $v \in P$ its $K$ neighbors with largest degree are selected and
(ii) \textbf{Weighted degree (WD) sensors} heuristic, which is similar to the
previous heuristic except that the $K$ neighbors are chosen based on largest
weighted degree \hao{what is the weight here?}.

%\subsection*{Estimating Lead Time}
%\label{evaluation}
Our primary figure of merit is the lead time, calculated as follows. For each
run of a disease model in a social contact network, we fit a logistic function
curve to the cumulative incidence of the chosen sensor set and a randomly
sampled set from $V$. We then derive daily incidence curves for both the sensor
set and the random set (we will refer this set as random set in the rest of this
paper). Let $t_s$ and $t_r$ represent the peak times of the daily incidence
curves for the sensor and random sets respectively, and the lead time is defined
as $\Delta t = t_r - t_s$.

\subsection*{Performance for the MAIT problem (infection time)}
\label{infection-time}
\hao{A lot of issues for the results. 1. what is the parameter settings for the
disease simulation? 2. Why is there only results on \miami{} dataset? What about
the results on the other two dataset? 3. How many independent runs did you
perform to get these results?}

Recall that by solving MAIT problem we aim to design a sensor set such that the
average infection time across the contact network $G$ is minimized with
sufficient probability. In this experiment, we set $k=5\%$ of the entire
population and infected five random nodes to initialize the epidemic simulation.
Fig.~\ref{fig:shift-vs-size} depicts the average infection time of the random
set and the sensor set chosen by four heuristics on \miami{} dataset. As we can
see, our proposed methods perform much better here on the \miami{} dataset. The
average infection time of sensor set selected by transmission tree (TT) and
dominator tree (DT) approaches have a significant lead compared to that of
random set. While, the lead time of sensor set in the baseline methods (Top-3
and WD) are very small or even zero. This is because, as described earlier, our
approaches precisely try to pick nodes with earlier expected infection times
among the ones that have a high enough probability of getting infected as social
sensors. \tozammel{we need to do the same experiments for Oregon and Seattle
datasets.}

\begin{figure}[!h]
	\centering
	\includegraphics[width=2in]{figs/Fig4a}
	\includegraphics[width=2in]{figs/Fig4}
	\caption{\textbf{Daily incidence of sensor sets selected by the heuristic
		approaches compared to the true daily incidence in the simulated
		epidemic on (a) \oregon~dataset (left), (b) \miami~dataset (right).}}
	\label{fig:shift-vs-size}
\end{figure}

%\tozammel{I think following paragraph should go to the next subsection}
Fig.~\ref{fig_dailyInc4} illustrates the \hao{peak times of the daily incidence
curves?} for sensor set and random set for \miami~dataset. The Dominator tree
and Transmission tree peaks lead the ground truth peak earliest, with a lead
time of approximately 12 days with variance 0.48. The weighted degree algorithm
comes third, with a lead time of approximately 4 days while the top-3 degree
sensor group peak occurs at almost the same time as the ground truth.\hao{we
need the same plots for Seattle datasets.}

\begin{figure}[!h]
	\centering
	\includegraphics[width=2in]{figs/Fig5a}
	\includegraphics[width=2in]{figs/Fig5}
	\caption{\textbf{The expected peak time of the daily incidence curve on
		(a) \oregon~dataset (left), (b) \miami~dataset (right).} Here Top-3, WD,
		TT, and DT denote Top-3 high degree, Top-3 weighted degree, Transmission
		tree based, and Dominator tree based heuristic respectively. }
	\label{fig_dailyInc4}
\end{figure}
%% Comparison of mean lead time 
%\input{tables/tab_comparison.tex}

\subsection*{Performance for the PLTM problem (lead time)}
\label{lead-time}
Our ultimate goal is to understand how our algorithms perform w.r.t.\ the peak
lead time problem (PLTM). In this experiment, we initialize simulation
parameters as follows. For each dataset, we set the number of seed nodes (i.e.
the number of nodes initially get infected in the epidemic) to be 1, 5 and 10.
The size $k$ of sensor set and random set is set at $5\%$ of the total
population. The flu transmission rate is $0.05$ for the SI model in the
\oregon{} dataset. Table~\ref{tab_bcmp} shows the mean lead time of the four
sensor set selection strategies under consideration over the \oregon~and
\miami~datasets. The experiment results demonstrate that our approaches
consistently outperform the other two baseline methods except for the \oregon{}
dataset. \hao{here, use a different disease model and flu transmission rate on
the \oregon~dataset}

Comparing the graph statistics of \oregon~dataset with \miami~and
\seattle~datasets shown in Table~\ref{tab:dataset}, we can observe that the
graph in the \oregon~dataset has a quite different topology structure from the
graphs in the \miami~and \seattle~datasets. The graph in the \oregon~dataset has
relatively small average degree but very large maximum degree, which indicates
this graph has star-like topology where few of the central vertices have very
large degrees. On the other hand, many vertices in the graphs of the \miami~and
\seattle~datasets have large degrees, and they spread all over the entire graph.
Thus, for the top-K degree based sensor selection approach, it is relatively
easy to include the central vertices with high degrees into the sensor set in
\oregon~dataset, but for the transmission tree and dominator tree based
approaches, whether the high degree vertices are included into the sensor set
will heavily depend on the choices of initial seeds of the epidemics in the
\oregon~network. Such central vertices with high degree are usually very
important for the epidemics in such star-like networks, which explains why the
top-K degree approach works better than the transmission tree and dominator tree
approaches. On the contrary, in the \miami~and \seattle datasets, the total
number of vertices is large, and it is quite difficult for top-K degree approach
to select sensors that could represent the entire graph only based on the local
friend-friend information. However, the transmission tree and dominator tree
based sensor selection strategies take the global epidemic spread information
into account, which chooses the sensor set that could represent the entire
graph. That's why they perform better in term of the lead time than the top-K
degree based approach on the large simulated US city networks. The results of
this experiment further demonstrate that the network topology must be considered
when designing social sensor selection strategies. \hao{not sure whether such
analysis is appropriate.}

\begin{table}[!h]
	\centering
	\caption {\textbf{Comparison of mean lead time across four different
		algorithms over three datasets using a fixed sensor size (5\% of the
		population).}}
	\label{tab_bcmp}
	\begin{tabular} {|l|c|c|c|c|c|c|c|c|}
		\hline
		Dataset & Seed & \multicolumn{4}{c|}{Mean lead time} \\  \cline{3-6} 
		&  & Top-K degree & Weight degree  & Transmission tree & Dominator tree
		\\ \hline
		%\oregon &1& 7.84  & n/a  & 0.17  & 2.21 \\ \hline
		%& 5 & 7.96 & n/a  & 0.40 & 1.45 \\ \hline
		%& 10 & 8.01 & n/a & 0.59 & 1.07\\
		\oregon &1& 13.13 & n/a  & 10.10  & 9.91 \\ \hline
		& 5 & 8.85 & n/a  & 7.93 & 7.75 \\ \hline
		& 10 & 11.00 & n/a & 8.63 & 8.55 \\
		\hline
		\miami &1& 0.29  & 3.38  & 10.46  & 10.08 \\ \hline 
		& 5 & 0.39 & 3.41  & 10.15  & 10.19 \\ \hline
		& 10 & 0.62 & 3.41 & 10.13 & 10.13\\
		\hline
		%\seattle
		%& 1 & -0.19   & 4.01  & 8.96  & 8.91 \\ \hline
		%& 5 & -0.15 & 3.98 & 8.90  & 8.84 \\ \hline
		%& 10 & -0.14 & 3.64 & 8.92 & 8.84\\
		%\hline
	\end{tabular}
\end{table}

\subsection*{How many sensors to choose?}
\label{sensor-size}
Since we have already demonstrated the influences of the network topology on
social sensor selection strategies, we will focus on the simulated social
contact network datasets for US cities in the rest of the experiments. An
interesting conundrum is the number of sensors to select in a design. In this
experiment, we randomly select 5 vertices as the initial seeds of the epidemic
simulation. Fig.~\ref{size_miami} depicts the mean
lead time and the inverse of variance-to-mean ratio of the lead time v.s.\
the sensor size for the \miami{} and \seattle{} datasets. The results show that
the variance of the lead time estimate is high for small size of sensor sets
and decreases as the sensor set size increases. This suggests a natural
strategy of scaling the lead time against the variance, thus helps establish a
sweet spot in the trade-off. This variance-to-mean ratio is also known as the
{\it Fano factor}, which is widely used as an index of dispersion. In both
datasets, there is a clear peak in the figure of the inverse of
variance-to-mean ratio, which suggests a suitable size of sensors to pick. 

\begin{figure}[!h]
	\centering
	\includegraphics[width=2in]{figs/Fig6a}
	\includegraphics[width=2in]{figs/Fig6b}
	\caption{\textbf{Mean lead time (left) and variance-to-mean ratio (right)
		v.s.\ the sensor size for the \miami{} dataset.} When sensor set size is
		less than $1.0\%$ of the entire population we observe higher (good) lead
		time, but also with high variance. Scaling the mean lead time by the
		variance, i.e., the reciprocal of the Fano factor, demonstrates a clear
		peak at approximately 20 sensors, the position where we can obtain
		substantial gains in lead time with correspondingly low variances.}
	\label{size_miami} 
\end{figure}

%\begin{figure}[!h]
	%\centering
	%\includegraphics[width=2in]{figs/Fig7a}
	%\includegraphics[width=2in]{figs/Fig7b}
	%\caption{\textbf{Mean lead time (left) and variance-to-mean ratio (right)
		%v.s.\ the sensor size for the \seattle{} dataset.} When sensor set size
		%is less than $2.0\%$ of the entire population we observe higher (good)
		%lead time, but also with high variance. Scaling the mean lead time by
		%the variance, i.e., the reciporcal of the Fano factor, demonstrates a
		%clear peak at approximately 10 sensors, the position where we can obtain
		%substantial gains in lead time with correspondingly low variances.}
	%\label{size_seattle} 
%\end{figure}

% Todo: revise the following text
\subsection*{Effect of the disease parameters on the lead time}
\label{disease-param}
\hao{What are the values for other model parameters? E.g.\ sensor set size?
	$\epsilon$? We need the similar figure for the \seattle~dataset.}
%For the \oregon{} dataset, in order to compare the lead time between the sensor
%and random groups, we increase the disease transmission probability steadily
%from $0.01$ to $1.0$ and monitor for 300 days. As Fig.~\ref{fig_parameter} (left)
%demonstrates, with the increase of disease transmission probability, the lead
%time decrease rapidly. This is reasonable due to the SI model assumptions since
%the more infectious the disease is, the faster it will spread in the overall
%population, and consequently the distinction of the epidemic peak time between a
%carefully picked sensor set and random set will become smaller.
In this experiment, we fix the size of sensors at $5\%$ of the entire
population, and measure the influences of the disease transmission parameter to
the predicted epidemic lead time.  Fig.~\ref{fig_parameter} (right) provides the
results on the \miami{} dataset with the \seir{} transmission model. We observe
a more stable lead time advantage.
%\hao{Here, why did you use SI model on \oregon{} dataset and \seir{} model on \miami{}
%dataset? The disease model is changing and dataset is also changing, which makes
%the results not comparable.}
\begin{figure}[!h]
	\centering
	%\includegraphics[width=2in]{figs/Fig8a}
	\includegraphics[width=2.5in]{figs/Fig8b}
	\caption{\textbf{Lead time v.s.\ disease transmission probability on
		\miami~(left) and \seattle~(right) datasets.} The size of sensor set and
		random set is fixed at 5\% of the entire population.}
	\label{fig_parameter}
\end{figure}

\subsection*{Empirical study on stability of lead time}
\label{stability}
%\hao{The description of the experiment you did is vague. The reviewer will not
%understand what you did in this experiment and why you did this experiment. And
%again, there is no description of parameter settings for this experiment. Shall
%we remove this section?} 
In this experiment, we study the stability of the estimated lead time as we
observes more data on the sensor group when the monitoring days increases. As is
well known, the cumulative incidence curve of flu epidemics can be perfectly
modeled by logistic function where the dependent and independent variables are
flu cumulative incidence and time of the epidemic (days in our context). Here,
we varied our flu epidemic simulation time from 2 days to 300 days on the
\miami~dataset, estimate cumulative incidence curves (the logistic function) for
both sensor and random group based on the simulated cumulative flu incidence
data, and then compute the lead time for each set of the simulation.
Fig.~\ref{fig_fitPredict} (a) shows the lead time v.s.\ the flu epidemic
simulation time. As we can see from this figure, the estimated lead time
fluctuates a lot when the simulation time is short and stabilizes at 12 days
when the epidemic simulation time is more than around 80 days. Such results
provide some insights for public health officials on how much epidemic data they
should collect in order to make an accurate estimation of the flu outbreak from
the time domain perspective.

\begin{figure}[!h]
	\centering
	\includegraphics[width=1.8in]{figs/Fig9a}
	\includegraphics[width=1.6in]{figs/Fig9b}
	\includegraphics[width=1.6in]{figs/Fig9c}
	\caption{(a) The estimate of the lead time fluctuates initially with
		increase in monitoring days and stabilizes quickly. (b and c)
		Multidimensional logistic regression plots enable us to relate the
		incidence profiles from the sensor set to the full population.}
	\label{fig_fitPredict}
\end{figure}

\subsection*{Predicting population epidemic curve from sensor group epidemic
curve} 
\label{predict-epicurve}
In the previous study, we show that logistic function can be used to fit
cumulative incidences very well. In this experiment, we perform a direct
regression between the observed epicurve over the sensor group and the random
group. Over 300 days, we use the first 110 days data on the sensor set (using
the dominator tree) to predict the remaining 180 days of cumulative incidence,
using a logistic regression. Fig.~\ref{fig_fitPredict} (b) and (c) compares the
cumulative incidence and daily incidence of sensor group by dominator tree from
\miami{} and as can be seen provides a very compelling fit. \hao{This experiment
needs further explanation, e.g.\ how do you fit the logistic function? What is
the loss function? Otherwise, I don't think the reviewer will understand
the purpose of this experiment. This section is also terrible. Should we delete
it?}


\subsection*{Surrogates for social sensors}
\label{surrogates}
All of the our proposed approaches (TT and DT heuristics) and the previous
experiments we have conducted are based on an importation assumption that we
know the detailed structure of the social contact network. Thus, we are able to
analyze the network structure, and identify the good sensor nodes by direct
inspection. However, in reality, the structures of large scale social contact
networks are usually unknown or difficult to obtain, which makes it difficult to
directly apply our proposed methods.

In order to make the proposed approaches deployable and solve realistic public
health problems, we now relax this key assumption, and try to find a {\it
surrogate} approach to select social sensors. In this case, the policy makers
can implement their strategies without detailed (and intrusive) knowledge of
people and their activities. Surrogates are thus an approach to implement
privacy-preserving social network sensors.

The key idea of our surrogate approach is to utilize the demographic
information. Here, we use \miami{} dataset as an example to explain our
surrogate approach. We extracted the following 16 demographic features from
\miami{} dataset:
\begin{itemize}
	\item Age, gender, and income
	\item Number of meetings with neighbor nodes
	\item Total meeting duration with neighbor nodes
	\item Number of meetings whose durations are longer than 20000 seconds
	\item Number of meetings of types 1--5
	\item Percent of meetings of types 1--5
\end{itemize}
The meeting types of 1--5 refer to home, work, shop, visit and school,
respectively. Among all these features, we first identify the differences of
the feature distributions between the sensor set selected by the proposed
transmission tree (or dominator tree) based heuristic when the
network structure is known and the random set. Large difference indicates that
the corresponding demographic feature characterizes the sensor set, thus could
be used to select surrogate sensors. Table X \hao{add the KL table here.} shows
the KL-divergence between the empirical distributions of sensor set and random
set for the extracted 16 demographic features from \miami~dataset. Here, we
chose the features of \emph{Age}, \emph{Total meeting duration with neighbor
nodes} and \emph{Meeting types} to help select surrogate sensors since they have
the largest KL-divergence as shown in Table X.  Fig.~\ref{figg1},
Fig.~\ref{figg2} and Fig.~\ref{figg3} compare the empirical distributions of
sensor set and random set for the three selected features for \miami~dataset.
\hao{Here, only comparing the distributions of these three features won't
demonstrate that these three are the most important feature to use to select
surrogates.  There should be a table of KL divergences comparing all the
demographic features appeared in the datasets. Also, change the Y-axis of the
following three figures into relative frequency.}

\begin{figure}[!h]
	\centering
	\includegraphics[width=2in]{figs/Fig10a}
	\includegraphics[width=2in]{figs/Fig10b}
	\caption{\textbf{Distribution of ages for sensor groups (left) and random
		groups (right).}}
	\label{figg1}
\end{figure}

\begin{figure}[!h]
	\centering
	\includegraphics[width=2in]{figs/Fig11a}
	\includegraphics[width=2in]{figs/Fig11b}
	\caption{\textbf{Distribution of total meeting duration time with neighbor
		vertices for sensor groups (left) and random groups (right).}}
	\label{figg2}
\end{figure}

 % meet type distribution
\begin{figure}[!h]
	\centering
	\includegraphics[width=2in]{figs/Fig12a}
	\includegraphics[width=2in]{figs/Fig12b}
	\caption{\textbf{Distribution of meeting types for sensor groups (left) and
		random groups (right).} The meeting types in the datasets correspond to
		home (1), work (2), shop (3), visit (4), school (5), and other (6).  }
	\label{figg3}
\end{figure}

Using the three identified features, we derived the following three criteria to
choose surrogate sensors:
\begin{itemize}
	\item People must come from the age group of 5-20 years (Fig.~\ref{figg1}).
	\item At least $80\%$ of the meetings with the neighbor nodes must have
		duration greater than $20,000$ seconds (Fig.~\ref{figg2} ).
	\item At least $80\%$ of the meetings with the neighbor nodes must be type
		$2$ or $5$ (Fig.~\ref{figg3}).
\end{itemize}
Applying these three criteria to the entire population of \miami{} dataset, we
obtained a surrogate sensor set $S^{\prime}$ of size $211,397$, which is still
too large to monitor compared to the typical survey size, e.g.\ $2,000$, in the
public heath studies. Thus, we need more rigorous criteria to further refine the
surrogate sensors. 

Here, we apply the classification and regression tree (CART) method. It
should be pointed out that although we choose CART algorithm there, 
any other supervised classification algorithm (e.g., decision trees) can also be
used to refine the surrogate social network sensors. The 16 attributes mentioned
above are used as dependent \hao{independent?} variables in our CART model, and
the response variable is binary to indicate whether a person should be selected
as a sensor or not. In order to learn the CART model, we created the training
data as follows. We chose $0.1\%$ of the entire population ($\approx 2000$) in
\miami{} dataset with our proposed heuristics as the training data with positive
responses (social sensors), and chose another $0.1\%$ randomly as the training
data with negative responses (not social sensors). Then, separate CART models
were learned to refine the surrogate sensor set $S^{\prime}$ for each
transmission rate ranging from $3.0 \times 10^{-5}$ to $5.5 \times 10^{-5}$ with
a step size of $5 \times 10^{-6}$. Such transmission rates are the typical
values used in various flu epidemic studies. Each of these CART models selected
approximate $30,000$ individuals as surrogate sensors from $S^{\prime}$, and we
chose the common individuals across all the CART models as the final surrogate
sensor set $S^{\prime\prime}$ whose size was $17,393$.

Fig.~\ref{fig_surrogate_cart} compares the mean lead time estimated with
surrogate sensor set $S^{\prime \prime}$ and sensor set selected with dominator
tree heuristic for various flu transmission rates. As we can see from
Fig.~\ref{fig_surrogate_cart}, although the surrogate sensor set
$S^{\prime\prime}$ selected by CART algorithm does not perform as well as the
proposed dominator tree based social sensors, it still provide a significant
mean lead time, which is good enough to give early warning to the public health
officials for the potential incoming flu outbreak. Most important, since the
CART based surrogate sensor approach does not require the information of the
social contact network structures, it is easy to implement and deploy in reality
compared to the transmission tree or dominator tree based heuristic approach.
This makes it a promising candidate for predicting flu outbreaks for public
health officials.
\begin{figure}[!h]
	\centering
	\includegraphics[width=3in]{figs/Fig13}
	\caption{\textbf{Mean lead times estimated with surrogate sensor set
		$S^{\prime \prime}$ and dominator tree based social sensors for various
		flu transmission rates.}}
	\label{fig_surrogate_cart}
\end{figure}

\subsection*{What information should be used to select surrogate sensors?}

Notice that in the last section, when we select the surrogate sensors with CART
algorithm, both demographic (e.g.\ age of individuals) and interaction (e.g.\
total meeting duration and meeting types with neighboring individuals)
information is taken into account. However, which kind of information is more
important in term of estimating the lead time of flu epidemics? What information
should be collected first if the resources are limited for public health
officials? Such practical issues need to be considered when developing surrogate
sensor selection strategies. In this section, we study how the individual
demographic and interaction information influence the estimated flu epidemic
lead time.

In this experiment, besides the \miami~datasets we used in the last experiment,
we include five other synthetic but realistic social contact network datasets
for large US cities, e.g.\ \boston,~\dallas,~\chicago,~\losangeles~and \newyork.
For each city, we selected the sensor set and the random set with fixed size of
$10,000$. The sensor set were selected with the following six strategies: 1)
using empirical distributions of demographic information (distr demo); 2) using
empirical distributions of interaction information (distr inter); 3) using CART
with demographic information (CART demo); 4) using CART with interaction
information (CART inter); 5) using CART with both demographic and interaction
information (CART demo+inter); 6) using transmission tree or dominator tree
based heuristic (trans or dom). We computed the mean lead time for each of the
six sensor selection strategies mentioned above, and the results were averaged
across $100$ independent runs. Figure~\ref{fig_surrogate_lead} shows the lead
time of the different approaches over the six US city datasets. As we can see
from the figure, our proposed approaches (CART based approaches and
transmission/dominator tree based approaches) outperforms the two baseline
methods (distr demo/inter), and in general, as more information is taken into
account, the larger estimated lead time could be achieved (since the
transmission/dominator tree based heuristics assume known social contact network
structures, they could be thought of possessing the most information about
epidemics). Furthermore, the individual interaction information seems to be more
important than the demographic information from the perspective of obtaining
larger lead time. Such findings provide some general guidelines for public
health officials on how to design surveys to collect public data in order to
predict flu epidemics.

\begin{figure}[!h]
	\centering
	\includegraphics[width=2.5in]{figs/Fig14a}
	\includegraphics[width=2.5in]{figs/Fig14b}
	\caption{\textbf{The lead time of transmission tree based (left) and
		dominator tree based (right) sensor selection strategies using different
		combinations of individual demographic and interaction information on
		Miami, Boston, Dallas, Chicago, Los Angeles and New York City
		datasets.}}
	\label{fig_surrogate_lead}
\end{figure}
