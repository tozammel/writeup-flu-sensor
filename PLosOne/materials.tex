\section*{Materials and Methods}
\label{background}
We present some fundamental concepts and our methods in this section.

\subsection*{Epidemiology Fundamentals}
The most fundamental computational disease model is the so-called
`Susceptible-Infected' (SI) model.  Each individual (e.g.\ node in the disease
propagation network) is considered to be in one of two states: Susceptible
(healthy) or Infected. Any infected individual may infect each of its neighbors
\emph{independently} with probability $\beta$. Also, the SI model assumes every
infected individual stays infected forever. If the disease propagation network
is a clique of $N$ nodes, the dynamic process of SI model can be written as: 
$$
\frac{d I}{dt} = \beta \times (N- I) \times I
$$
where $I$ is the number of infected nodes at time $t$. The justification is as
follows: there are a total of $I (N-I)$ encounters between an infected node and
a susceptible node, and each of these encounters are successful with
a probability $\beta$. It is easy to confirm that the solution for $I$ is the
logistic or sigmoid function, and its derivative (or the number of \emph{new}
infections per unit time) is symmetric around the peak.

Another popular disease model that we use in this paper is the so-called SEIR
model where a node in the disease propagation network is in one of \emph{four}
states, corresponding to Susceptible-Exposed-Infected-Recovered; this approach
models diseases with a latent exposed phase, during which an individual is
infected but not infectious, and a cured or recovered phase where the infected
individuals are healed and considered to be immune to the disease under
consideration.

\subsection*{Social Sensors for Disease Outbreaks}
A social sensor is a set of individuals selected from the population which
could indicate outbreak of the disease under consideration, thus give early
warnings. Christakis and Fowler~\cite{christakis:10:sensor} first proposed the
notion of social network sensors for monitoring flu based on the friendship
paradox: your friends have more friends than you do. Alternatively, it can be
represented that a friend of a random person has higher mean degree than that of
the random person in the friendship network. They proposed to use the set of
nominated friends by randomly choosing people from the population as the set of
social sensors. 

Motivated by complicated public health concerns during the initial stages of a
pandemic (other than just detecting if there is an epidemic at
all)~\cite{nsubuga06}, public health officials are usually interested in the
questions: will there be a large disease outbreak? Or, has the epidemic reached
its peak? These are important questions from a public health
perspective~\cite{cdc-flu}; it can help determine if costly interventions are
needed (e.g., school closures), the strategies to organize vaccination
campaigns, locations to prioritize efforts to minimize new infections, the time
to issue advisories, and in general how to better engineer health responses.

Inspired by the concept of social sensors, in this paper, we cast such public
health concerns as a disease outbreak prediction problem with social sensors,
To be more specific, Let $G = (V, E)$ be a social contact network where $V$ and
$E$ represent the vertex set and edge set respectively, and we focus on SEIR
process here, but the formulations extend naturally to the SI model.  We use
$f(S)$ to denote the probability that at least one node in sensor set $S$ gets
infected, starting the disease spread from a random initial node. 

The most basic problem in such a setting is the \emph{early detection} problem,
in which the goal is to select the smallest sensor set $S$ so that some node in
$S$ gets infected within the first $d$ days of the disease outbreak in $G$ with
probability at least $\epsilon$ (here, $d$ and $\epsilon$ are given
parameters)---this can be used to detect if there is an epidemic at all. This
problem can be viewed as a special case of the detection problem in
\cite{Leskovec@KDD07}, and can be solved within a constant factor by a greedy
submodular function maximization algorithm. As we show later, our optimization
goal is \textit{non-linear} and \textit{not submodular}, and hence the approach
in ~\cite{Leskovec@KDD07} can not be directly applied. Importantly, the early
detection problem does not capture the more important issues about the disease
characteristics of relevance to public health officials, and therefore we do not
explore this further. For example, just detecting an infection in the population
is generally not enough reason for actually doing an expensive intervention by
the public health officials (as the disease might not spread and disappear
soon). But knowing that the infection will still grow further and peak, gives
justification for robust infection control measures.

In our formulation, we refer the term \emph{epicurve} $I(t)$ as the time series
of the number of infections by day. The \emph{peak} of an epicurve is its
maximum value i.e.\ $\max_t I(t)$. Note that it is possible for a epicurve to
have multiple peaks, but for most epidemic models in practice, the corresponding
epicurves usually have a single peak. The derivative of the $I(t)$ with respect
to $t$ is called the \emph{daily incidence} curve (number of new infections per
day). The ``time of peak'' of the epicurve corresponding the entire population
is time when the epicurve first reaches its peak, and is denoted by $t_{pk} =
\argmax_t I(t)$.  Similarly, we use $t_{pk}(S)$ to denote the time-of-peak of
the epicurve restricted only to a set $S$. The lead time of the epicurve peak
for sensor set $S$ compared to the entire population is then simply $t_{pk} -
t_{pk}(S)$. The problem we study in this paper is: 
\begin{quote}
	\textbf{$(\epsilon, k)$-Peak Lead Time Maximization (PLTM)} \\
	\textbf{\emph{Given:}} Parameters $\epsilon$ and $k$, network $G$, and the
	epidemic model \\
	\textbf{\emph{Find:}} A set of nodes $S$ from $G$ such that  
	\begin{align*}
		S = & \argmax_S  E[t_{pk}-t_{pk}(S)] \\
		\mbox{s.t.} &~f(S)\geq\epsilon,~|S|=k
	\end{align*}
\end{quote}
Here, $k$ is the budget, i.e.\ the required size of sensor set. Notice that we
need the $f(S)$ constraint so that we only choose sets which have a minimum
probability of capturing the epidemic---intuitively, there may be some nodes
which only get infected infrequently, but the time they get infected during the
disease propagation might be quite early. Such nodes are clearly not good
`sensors'.

\subsection*{Proposed Approach}
Unfortunately, the peak of an epicurve is a high variance measure, making it
challenging to address directly. Further, the expected lead time,
$E[t_{pk}-t_{pk}(S)]$ is not non-decreasing and non-submodular, in general.
Hence we consider a different, but related problem, as an intermediate step.
Let $t_{\mathit{inf}}(v)$ denote the expected infection time for node $v$, given
that the epidemic starts at a random initial node. Then:
\begin{quote}
	\textbf{$(\epsilon, k)$-Minimum Average Infection Time (\textsc{MAIT})} \\
	\textbf{\emph{Given:}} Parameters $\epsilon$ and $k$, network $G$, and the
	epidemic model \\
	\textbf{\emph{Find:}} A set $S$ of nodes such that  
	\begin{align*}
		S = & \argmin_S  \sum_{v\in S} t_{\mathit{inf}}(v)/|S| \\
		\mbox{s.t.} &~f(S)\geq\epsilon,~|S|=k
	\end{align*}
\end{quote}

\par \noindent
\textbf{Justification:} In contrast to the peak, note that the \emph{integral}
of the epicurve restricted to $S$, normalized by $|S|$, corresponds to the
\emph{average infection time} of nodes in $S$, which is another useful metric
for characterizing the epidemic. Further, if the epicurve has a sharp peak,
which happens in most real networks, and for most disease parameters, the
average infection time is likely to be close to $t_{pk}$. 

%\subsection{Approximating MAIT}
%\paragraph{Approximating MAIT:}
\textbf{Approximating MAIT:}
The MAIT problem involves $f(S)$, which can be seen to be submodular, following
the same arguments as in \cite{Kempe03Maximizing}, and can be maximized using a
greedy approach.  However, the average infection time constraint $\sum_{v\in S}
t_{\mathit{inf}}(v)/|S|$ is non-linear as we keep adding nodes to $S$, which
makes this problem challenging, and the standard greedy approaches for
maximizing submodular functions, and their extensions~\cite{Krause@ICML08} do
not work directly. In particular, we note that selecting a sensor set $S$ which
minimizes $\sum_{v\in S} t_{\mathit{inf}}(v)$ (with $f(S)\geq\epsilon$) might
not be a good solution, since it might have a high average infection time
$\sum_{v\in S} t_{\mathit{inf}}(v)/|S|$.  We discuss below an approximation
algorithm for this problem.  For graph $G=(V,E)$, let $m=|E|$, $n=|V|$.

\begin{lemma}
\label{lemma:mait}
	It is possible to obtain a bi-criteria approximation $S\subseteq V$ for any
	instance of the $(\epsilon,k)$-\textsc{MAIT} problem on a graph $G=(V,E)$,
	given the $t_{\mathit{inf}}(\cdot)$ values for all nodes as input, such that
	$\sum_{v\in S} t_{\mathit{inf}}(v)$ is within a factor of two of the
	optimum, and $f(S)\geq c\cdot\epsilon$, for a constant $c$.  The algorithm
	involves $O(n^2\log{n})$ evaluations of the function $f(\cdot)$.
\end{lemma}

\begin{proof}(Sketch)
	Let $t_{\mathit{inf}}(v)$ denote the expected infection time of $v\in V$,
	assuming the disease starts at a random initial node. Let $B_{\mathit{opt}}$
	be the average infection time value for the optimum; we can ``guess'' an
	estimate $B'$ for this quantity within a factor of $1+\delta$, by trying out
	powers of $(1+\delta)^i$, for $i\leq\log{n}$, for any $\delta>0$, since
	$B_{opt}\leq n$. We run $O(\log{n})$ ``phases'' for each choice of $B'$.

	Within each phase, we now consider the submodular function maximization
	problem to maximize $f(S)$, with two linear constraints: the first is $\sum
	t_{\mathit{inf}}(v)x(v)\leq B'k$ and $\sum_v x(v)\leq k$, where $x(\cdot)$
	denotes the characteristic vector of $S$. Using the result of Azar et al.
	\cite{azar:icalp12}, we get a set $S$ such that $f(S)\geq c\mu(B')$, for a
	constant $c$, and $\sum_{v\in S} t_{\mathit{inf}}(v)\leq B'k$ and $|S|\leq
	k$, where $\mu(B')$ denotes the optimum solution corresponding to the choice
	of $B'$ for this problem. If we have $|S|<k$, we add to it $k-|S|$ nodes
	with the minimum $t_{\mathit{inf}}(\cdot)$ values, which are not already in
	$S$, so that its size becomes $k$. Note that for the new set $S$, we have
	$\sum_{v\in S} t_{\mathit{inf}}(v)\leq 2B'k$, since the sum of the infection
	times of the nodes added to $S$ is at most $B'k$.

	Note that the resulting set $S$ corresponds to one ``guess'' of $B'$. We
	take the smallest value of $B'$, which ensures $f(S)\geq c\epsilon$. It
	follows that for this solution $S$, we have $\sum_{v\in S}
	t_{\mathit{inf}}(v)/|S|\leq 2B_{opt}$ and $|S|=k$.  The algorithm of Azar et
	al. \cite{azar:icalp12} involves a greedy choice of a node each time; each
	such choice involves the evaluation of $f(S')$ for some set $S'$, leading to
	$O(n^2)$ evaluations of the function $f(\cdot)$; since  there are
	$O(\log{n})$ phases, the lemma follows.
\end{proof}

%\subsection{Heuristics}
%\label{sec:heuristics}
\paragraph{Heuristics}
\hao{Is there any proof or justification that the DT and TT will get the sensor
set that solve or approximately solve the MAIT problem?}

Though Lemma~\ref{lemma:mait} runs in polynomial time, it is quite impractical
for the kinds of graphs we study in this paper because of the need for
super-quadratic number of evaluations of $f(\cdot)$.  Therefore, we consider
faster heuristics for selecting sensor sets. The analysis of
Lemma~\ref{lemma:mait} suggests the following significantly faster greedy
approach: pick nodes in non-decreasing $t_{\mathit{inf}}(\cdot)$ order till the
resulting set $S$ has $f(S)\geq\epsilon$. In general, this approach might not
give good approximation guarantees. However, when the network has ``hubs'', it
seems quite likely that the greedy approach will work well. However, even this
approach requires repeated evaluation of $f(S)$, and can be quite slow. The
class of social networks we study have the following property: nodes $v$ which
have low $t_{\mathit{inf}}(v)$ are usually hubs and have relatively high
probability of becoming infected. This motivates the following simpler and much
faster heuristic, referred to as the \textbf{Transmission tree (TT) based
sensors} heuristic:
\begin{enumerate*}
	\item generate a set $\mathcal{T}=\{T_1,\ldots,T_N\}$ of dendrograms;
	\item for each node $v$, compute $d_{v}^{i}$, which is its depth in $T_i$,
		for all $i$, if $v$ gets infected in $T_i$;
	\item compute $t_{\mathit{inf}}(v)$ as the average of the $d_{v}^{i}$, over
		all the dendograms $T_i$ in which it gets infected;
	\item discard nodes $v$ with $t_{\mathit{inf}}(v)<\epsilon_0$, where
		$\epsilon_0$ is a parameter for the algorithm;
	\item order the remaining nodes $v_1,\ldots,v_{n'}$ in non-decreasing
		$t_{\mathit{inf}}(\cdot)$ order (i.e., $t_{\mathit{inf}}(v_1)\leq
		t_{inf}(v_2)\leq\ldots\leq t_{inf}(v_{n'})$) 
    \item let $S=\{v_1,\ldots,v_k\}$
\end{enumerate*} 

We also use a faster approach based on dominator trees, which is motivated by
the same ideas as greedy (referred to as the \textbf{Dominator tree (DT) based
sensors} heuristic): 
\begin{enumerate*}
	\item generate dominator trees corresponding to each dendrogram;
	\item compute the average depth of each node $v$ in the dominator trees (as
		in the transmission tree heuristic);
	\item discard nodes whose average depth is smaller than $\epsilon_0$;
	\item we order nodes based on their average depth in the dominator tree, and
		pick $S$ to be the set of the first $k$ nodes.
\end{enumerate*}
Formally, the dominator relationship is defined as follows. A node $x$ dominates
a node $y$ in a directed graph iff all paths from a designated start node to
node $y$ must pass through node $x$. In our case, the start node indicates the
source of the infection or disease. Consider Fig.~\ref{fig:domtree} (left), a
schematic of a social contact network; all paths from node A (the designated
start node) to node H must pass through node B, therefore B dominates H.  Note
that a person can be dominated by many other people.  For instance, both C and F
dominate J, and C dominates F.  A node $x$ is said to be the unique immediate
dominator of $y$ iff $x$ dominates $y$ and there does not exist a node $z$ such
that $x$ dominates $z$ and $z$ dominates $y$.  Note that a node can have at most
one immediate dominator, but may be the immediate dominator of any number of
nodes. The dominator tree $D = (V^D,E^D)$ is a tree induced from the original
directed graph $G = (V^G,E^G)$, where ${V^D} = {V^G}$, but an edge $(u
\rightarrow v) \in E^D$ iff $u$ is the immediate dominator of $v$ in $G$. Figure
~\ref{fig:domtree} (right) shows an example dominator tree.

The computation of dominators is a well studied topic and we adopt the
Lengauer-Tarjan algorithm ~\cite{LengTarjan} from the Boost graph library
implementation.  This algorithm runs in $O((|V|+|E|) \log (|V|+|E|))$ time,
where $|V|$ is the number of vertices and $|E|$ is the number of edges.

\begin{figure}[!h]
	\centering
	\includegraphics[width=4in]{figs/Fig3}
	\caption{\textbf{(i) An example graph and (ii) its dominator tree.} In
	practice, the dominator will have a significantly reduced number of edges
	than the original graph.}
	\label{fig:domtree}
\end{figure}
