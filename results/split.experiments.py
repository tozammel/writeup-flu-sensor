#!/usr/bin/python
#
# ---------------------------------------------------------------------------------------------------
# file: split.experiments.py
# function: extract duration data and 100 iterations of experiments data into separate files
# input: miami-NI.EFO
# author: HW
# ---------------------------------------------------------------------------------------------------

str_input = '../dataset/miami.oneseed/miami-RSF10.EFO'
str_durationOut = '../dataset/miami.oneseed/miami-RSF10.duration'

# write disease duration data
print 'processing the duration data...'
file_input = open(str_input, 'r')
file_duration = open(str_durationOut, 'w')

file_input.readline()
str_titleLine = file_input.readline()
file_duration.write(str_titleLine)

while True:
    str_line = file_input.readline()
    if not str_line:
        break
    if str_line.startswith('#'):
        break
    else:
        file_duration.write(str_line)
# end of loop while
file_duration.close()
str_titleLine = str_line      # title line for experiment data file
str_line = ''

# write the experiment data
print 'processing experiment data...'
list_iterations = range(0, 1000)
for iterNum in list_iterations:
    print 'processing experiment: ' + str(iterNum) + '...'
    str_expOut = '../dataset/miami.oneseed/miami-RSF10.experiments/' + str(iterNum) + '.experiment'
    file_expOutput = open(str_expOut, 'w')
    if str_line != '':
        file_expOutput.write(str_line + '\n')
    while True:
        str_line = file_input.readline()
        if not str_line:
            break
        str_line = str_line.strip()
        list_terms = str_line.split(' ')
        if int(list_terms[1]) == iterNum:
            file_expOutput.write(str_line + '\n')
        else:
            break
    # end of inner loop while
    file_expOutput.close()
# end of loop for

file_input.close()
