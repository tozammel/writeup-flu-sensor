#!/bin/sh

#PBS -l walltime=01:00:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=ndssl
#PBS -q ndssl_q
#PBS -A ndssl

#PBS -N HelloWorld
#PBS -j oe
#PBS -o logfile.qlog

##PBS -m ae
##PBS -M email@gmail.com

# If modules are needed, source modules environment:
. /etc/profile.d/modules.sh

# Add any modules you might require:

module load openmpi
module load bio/python/2.7.2

cd $PBS_O_WORKDIR

# Below here enter the commands to start your job

time mpirun -n 4 python myprogram.py > output.txt

#time mpiexec -n 4 python myprogram.py > output.txt

exit;
