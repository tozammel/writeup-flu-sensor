#!/usr/bin/env python

from mpi4py import MPI

comm = MPI.COMM_WORLD
mpisize = comm.Get_size()
rank = comm.Get_rank()
print 'Hi from processor', rank

