%\section{Related Work}
\section*{Discussion}
\label{sec:related}

There are several existing literatures on detecting outbreaks in networks. The
closest related work would be the approach proposed by Christakis and
Fowler~\cite{christakis:10:sensor}, where a simple heuristic that monitors the
friends of randomly chosen individuals from a social network as sensors was
adopted to achieve early detection of epidemics. However, they only demonstrated
their proposed approach on a relatively small social network, e.g.\ student
network from Harvard College. As we have shown earlier, their friend heuristic
fails on large social contact networks of US cities. We also demonstrated in
Result Section that although the Christakis and Fowler's approach works well
over the small network like \oregon~dataset, it almost provides no lead time
over large scale social contact network like \miami~dataset. To explain why the
proposed social sensor selection heuristics work better, we start from analyzing
the structures of the disease propagation networks. Comparing the graph
statistics of \oregon~dataset with \miami~dataset shown in
Table~\ref{tab:dataset}, we can observe that the graph in the \oregon~dataset
has a quite different topology structure from the graphs in the \miami~datasets.
The graph in the \oregon~dataset has relatively small average degree but very
large maximum degree, which indicates this graph has star-like topology where
few of the central vertices have very large degrees. On the other hand, many
vertices in the graphs of the \miami~datasets have large degrees, and they
spread all over the entire graph. Thus, for the top-K degree based sensor
selection approach, it is relatively easy to include the central vertices with
high degrees into the sensor set in \oregon~dataset, but for the transmission
tree and dominator tree based approaches, whether the high degree vertices are
included into the sensor set will heavily depend on the choices of initial seeds
of the epidemics in the \oregon~network. Such central vertices with high degree
are usually very important for the epidemics in such star-like networks, which
explains why the top-K degree approach works better than the transmission tree
and dominator tree approaches. On the contrary, in the \miami~dataset, the total
number of vertices is large, and it is quite difficult for top-K degree approach
to select sensors that could represent the entire graph only based on the local
friend-friend information. However, the transmission tree and dominator tree
based sensor selection strategies take the global epidemic spread information
into account, which chooses the sensor set that could represent the entire
graph. That's why they perform better in term of the lead time than the top-K
degree based approach on the large simulated US city networks. The interesting
insight revealed by such results is that the network topology must be considered
when designing social sensor selection strategies. The results also demonstrate
that the proposed TT and DT based sensor selection heuristics are more robust to
the underlying network topologies, and thus more suitable to be deployed in
practice, such as monitoring and forecasting epidemics in large cities.

Other work on detecting outbreaks over networks, like Leskovec et.\
al.~\cite{Leskovec@KDD07}, defined objective functions with submodularity to
pick optimal locations to place sensors in water and blog networks, subject to
several metrics like population affected, and time-to-first-detection. In
contrast, our metrics are \emph{not} submodular, more complex (shifts in peak
time) and more realistic for biological epidemics, giving significant additional
time for reaction.

There are a lot of research interests in studying different types of
information dissemination processes on large graphs, including (a)
information cascades~\cite{Bikchandani:1992,Goldenberg:2001}, (b) blog
propagation~\cite{Leskovec:2007:sdma,Gruhl:2004,Kumar:2003,Richardson:2002}, and
(c) viral marketing and product penetration~\cite{Leskovec:2006:ec}. The
classical texts on epidemic models and analysis are May and
Anderson~\cite{andersonmay} and Hethcote~\cite{hethcote2000}. Widely-studied
epidemiological models include {\em homogeneous
models}~\cite{Bailey1975Diseases,McKendrick1926Medical,andersonmay} which assume
that every individual has equal contact with others in the population. Much
research in virus propagation studied the so-called epidemic threshold on
different types of graphs, that is, to determine the condition under which an
epidemic will not break
out~\cite{kephart1993,vespignani2001,deepay2008,ganesh05effect,Prakash@ICDM11}.

Detection and forecasting are fundamental and recurring problems in public
health policy planning, e.g.,~\cite{nishiura11, mckinley09, nsubuga06,
nsoesie11}. National and international public health agencies are actively
involved in syndromic surveillance activities to detect outbreaks of different
infectious diseases---such surveillance information could include confirmed
reports of infections, and estimates of the number of infections. In the initial
days of an outbreak, such information is very limited and noisy, and
understanding the true extent of the outbreak and its dynamics are challenging
problems, e.g.,~\cite{shmueli10}. As in the case of the swine flu pandemic a few
years back\cite{nsubuga06}, whether the epidemic has peaked, is a fundamental
problem. Some of the few papers \cite{nsoesie11, mckinley09} consider the
problems of estimating the temporal characteristics of an outbreak. They
use simulation based approaches for model based reasoning about epicurves and
other characteristics.

Another related problem is immunization, i.e.\ the problem of finding the best
vertices for removal to stop an epidemic, with effective immunization strategies
for static and dynamic graphs~\cite{Hayashi03Recoverable, Tong@ICDM10,
Briesemeister03Epidemic\hide{, prakash2010}}. Other such problems where we
wish to select a subset of `{\em important}' vertices from graphs, include
`finding most-likely culprits of epidemics'~\cite{lappas:10:effectors,
Prakash@ICDM12} and the influence maximization problem for viral
marketing~\cite{richardson2002mining, chen2009efficient,kimura2006tractable}. 
