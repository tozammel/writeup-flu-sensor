
%d1 = importdata('miami-5-seed/sensor_size_vs_shift.dat');
%size(data)
d1 = importdata('miami-5-seed/miami_size.dat');
%d1 = importdata('oregon_dt_size.dat');

%d1 = importdata('seattle_size.dat');

%d1

ndpoints=11;
%ndpoints=8;
%ndpoints=5;

x=d1(:,1);
x=x(1:ndpoints,1);

dom=d1(:,2); 
dom=dom(1:ndpoints,1);
stddom=d1(:,3);
stddom=stddom(1:ndpoints,1);
vardom=stddom.*2;
%vardom=d1(:,4);
%vardom=vardom(1:ndpoints,1);
y1=dom./vardom;

tree=d1(:,4);
tree=tree(1:ndpoints,1);
stdtree=d1(:,5);
stdtree=stdtree(1:ndpoints,1);
vartree=stdtree.*2;
%vartree=d1(:,7);
%vartree=vartree(1:ndpoints,1);
y2=tree./vartree;

ndpoints = 3
d2 = importdata('oregon_size.dat');
x1=d2(:,1);
x1=x1(1:ndpoints,1);

t3=d2(:,2); 
t3=t3(1:ndpoints,1);
stdt3=d2(:,3);
stdt3=stdt3(1:ndpoints,1);
vart3=stdt3.*2;
y3=t3./vart3;


fsize=18;



h_size = figure; clf;
set(h_size,'Units','normalized','Position',[0 0.3 0.5 0.5],'Name','Mean over Var vs Sensor Size');
%plot(data(:,1)/1000,data(:,2),'-b*','linewidth',2);

%plot(data(:,1),data(:,2),'-b*','linewidth',2);
plot(x,y1,'-b*','linewidth',2);
%hold on
%plot(x,y2,'--ro','linewidth',2);
%errorbar(x,dom,stddom,'-b*','linewidth',2);
%hold on
%plot(x1,y3,'-.gs','linewidth',2);

%plot(data(:,1),data(:,3),'-r+','linewidth',2);
%errorbar(x,tree,stdtree,'--ro','linewidth',2);
%errorbar(x,tree,stdtree,':ro','linewidth',2);

xlabel('Sensor Set Size (% of population)', 'FontSize', fsize);
ylabel('Mean/Variance', 'FontSize', fsize);


%titleStr = 'Mean Shift over Var vs Sensor Size';
%title(titleStr,'FontSize',fsize);
%h_legend = legend('Dominator Tree Sensors','Transmission Tree Sensors', 'Top-3 Degree');
%h_legend = legend('Dominator Tree Sensors','Transmission Tree Sensors', 'Top-3 Degree');
%set(h_legend, 'FontSize', fsize);
%set(h_legend, 'Location', 'NorthWest');
%}

%{
set(gcf, 'PaperPosition', [1 1 58 58]);
set(gcf, 'PaperSize', [59 59]);
fig1name='shift_vs_size.pdf';
print(h_size,'-dpdf',fig1name)
%}



%h1=figure(2);clf;
