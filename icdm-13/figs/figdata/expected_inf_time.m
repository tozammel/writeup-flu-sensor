

y = [ [146.63,146.70]; [144.12,146.68]; [136.53,146.68]; [136.54,146.73]];

h_size = figure; clf;
set(h_size,'Units','normalized','Position',[0 0.3 0.5 0.5],'Name','Mean Shift Days vs Sensor Size');

bar(y)
fsize = 18;
ylabel('Expected Time of Infection', 'FontSize', fsize);

h_legend = legend('Sensors','Random');
set(h_legend, 'FontSize', fsize);
set(h_legend, 'Location', 'NorthEast');

%lab = ['Top-3', 'Weighted Degree', 'Transmission Tree', 'Dominator Tree'];
%set(gca,'XTickLabel',lab);