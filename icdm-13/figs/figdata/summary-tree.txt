#nodes in the network=2092076 
#edges in the network=
#exp that took off = 837
#Number of nodes that are infect in at least one exp  20823    36
Min accorrs all nodes =  0.0
Max accorrs all nodes =  67.0

tables:
------------------------------
0.1 21.83   21.59
0.5 17.71   17.92
1.0 15.06   15.17
2.5 12.36   12.35
5.0 10.19   10.15
7.5 8.53    8.46
10.0    7.36    7.34
20.0    4.39    4.37
30.0    2.80    2.83
40.0    1.97    1.99
50.0    1.34    1.34
60.0    0.87    0.88
70.0    0.50    0.50
80.0    0.25    0.23
90.0    0.11    0.11
95.0    -0.01   -0.01



details:
-----------------------------
00.1
    sensorSizePercent =  0.1
    sensorSize =  2092
    final occurrence th =  0.9
    final filtered list size =  25654
    min in filtered list =  0.0 max in filtered list=  35.2382198953
    Mean shift : 21.5923827778
    Min shift : 10.303788
    Max shift : 36.096468

00.5
    sensorSizePercent =  0.5
    sensorSize =  10460
    final occurrence th =  0.9
    final filtered list size =  25654
    min in filtered list =  0.0 max in filtered list=  35.2382198953
    Mean shift : 17.9198497431
    Min shift : 13.676928
    Max shift : 23.484405

01.0
    sensorSizePercent =  1.0
    sensorSize =  20921
    final occurrence th =  0.9
    final filtered list size =  25654
    min in filtered list =  0.0 max in filtered list=  35.2382198953
    Mean shift : 15.1688428961
    Min shift : 13.041133
    Max shift : 17.247334

02.5
    sensorSizePercent =  2.5
    sensorSize =  52302
    final occurrence th =  0.86
    final filtered list size =  58839
    min in filtered list =  0.0 max in filtered list=  35.7099447514
    Mean shift : 12.3549560502
    Min shift : 10.845334
    Max shift : 14.131045

05.0
    sensorSizePercent =  5.0
    sensorSize =  104604
    final occurrence th =  0.81
    final filtered list size =  113416
    min in filtered list =  0.0 max in filtered list=  36.4174041298
    Mean shift : 10.1517335998
    Min shift : 8.989939
    Max shift : 11.275028

07.5
    sensorSizePercent =  7.5
    sensorSize =  156906
    final occurrence th =  0.77
    final filtered list size =  161837
    min in filtered list =  0.0 max in filtered list=  36.6651234568
    Mean shift : 8.46358522103
    Min shift : 7.583645
    Max shift : 9.283327

10.0
    sensorSizePercent =  10.0
    sensorSize =  209208
    final occurrence th =  0.73
    final filtered list size =  212349
    min in filtered list =  0.0 max in filtered list=  37.019138756
    Mean shift : 7.34388164636
    Min shift : 6.712037
    Max shift : 8.112855

20.0
    sensorSizePercent =  20.0
    sensorSize =  418415
    final occurrence th =  0.57
    final filtered list size =  420679
    min in filtered list =  0.0 max in filtered list=  38.0554414784
    Mean shift : 4.37365658901
    Min shift : 3.983666
    Max shift : 4.76931

30.0
    sensorSizePercent =  30.0
    sensorSize =  627623
    final occurrence th =  0.42
    final filtered list size =  634837
    min in filtered list =  0.0 max in filtered list=  38.5408450704
    Mean shift : 2.82901794265
    Min shift : 2.56439
    Max shift : 3.107169


40.0
    sensorSizePercent =  40.0
    sensorSize =  836830
    final occurrence th =  0.31
    final filtered list size =  857873
    min in filtered list =  0.0 max in filtered list=  38.8797250859
    Mean shift : 1.99155354361
    Min shift : 1.769752
    Max shift : 2.235675

50.0
    sensorSizePercent =  50.0
    sensorSize =  1046038
    final occurrence th =  0.24
    final filtered list size =  1061699
    min in filtered list =  0.0 max in filtered list=  39.1074766355
    Mean shift : 1.33793284827
    Min shift : 1.137375
    Max shift : 1.541209

60.0
    sensorSizePercent =  60.0
    sensorSize =  1255246
    final occurrence th =  0.18
    final filtered list size =  1273184
    min in filtered list =  0.0 max in filtered list=  39.5988700565
    Mean shift : 0.879894758662
    Min shift : 0.690464
    Max shift : 1.085558

70.0
    sensorSizePercent =  70.0
    sensorSize =  1464453
    final occurrence th =  0.13
    final filtered list size =  1471805
    min in filtered list =  0.0 max in filtered list=  39.6174496644
    Mean shift : 0.475964641577
    Min shift : 0.283683
    Max shift : 0.692193

80.0
    sensorSizePercent =  80.0
    sensorSize =  1673661
    final occurrence th =  0.08
    final filtered list size =  1688199
    min in filtered list =  0.0 max in filtered list=  41.0147058824
    Mean shift : 0.23360316368
    Min shift : -0.001444
    Max shift : 0.489813

90.0
    sensorSizePercent =  90.0
    sensorSize =  1882868
    final occurrence th =  0.03
    final filtered list size =  1925927
    min in filtered list =  0.0 max in filtered list=  42.2692307692
    Mean shift : 0.114915057348
    Min shift : -0.233575
    Max shift : 0.351355

95.0
    sensorSizePercent =  95.0
    sensorSize =  1987472
    final occurrence th =  0.01
    final filtered list size =  2031637
    min in filtered list =  0.0 max in filtered list=  46.6153846154
    Mean shift : -0.012567958184
    Min shift : -0.391289
    Max shift : 0.51497




-------------------------------------------
10K:
    final occurrence th =  0.9
    final filtered list size =  25654
    min in filtered list =  0.0 max in filtered list=  35.25392670

100K:
    final occurrence th =  0.82
    final filtered list size =  100829
    min in filtered list =  0.0 max in filtered list=  35.94192634
    56
    Mean shift : 9.96364583035
    Min shift : 8.873564
    Max shift : 11.026799

150K:
    final occurrence th =  0.77
    final filtered list size =  161837
    min in filtered list =  0.0 max in filtered list=  36.66358024
    69
    Mean shift : 8.84225667503
    Min shift : 7.826953
    Max shift : 9.785723


