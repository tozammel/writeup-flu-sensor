\newcommand{\argmax}{\operatornamewithlimits{arg\ max}}
\newcommand{\argmin}{\operatornamewithlimits{arg\ min}}

\section{Problem Formulation}
\label{sec:formalisms}

We now discuss specific formulations for sensor set problems. These are motivated by
more complex public health concerns during the initial stages of a pandemic
(other than just detecting if there is an epidemic at all),
such as \cite{nsubuga06}: will there be a large outbreak, and has the epidemic
reached its peak?

We now describe the main problem we are trying to solve in this paper, viz.
determining whether the epidemic has reached its peak (and when
it might). This is one
of the important problems from a public health perspective \cite{cdc-flu}; 
it can help determine if costly interventions are needed (e.g., school closures),
how to organize vaccination campaigns, locations to prioritize efforts to
minimize new infections, how to leverage strained resources,
how to be judicious about costs,
when to issue advisories, and in general how to better engineer health
responses.

We use the term \emph{epicurve} as the time series of the number of infections
by day i.e. $I(t)$. The \emph{peak} of an epicurve is the maximum value i.e.
$\max_t I(t)$; note that it is possible for a graph to have multiple peaks, but
for most epidemic models in practice, we only have a single peak. The derivative
of the epicurve is called the \emph{daily incidence} curve (number of new
infections per day).
%(i.e., the time when
%the epicurve 
%first peaks)
The ``time of peak'' of the epicurve of the entire population is time when the
epicurve corresponding to the entire population first peaks and is denoted by
$t_{pk} = \argmax_t I(t)$; we use $t_{pk}(S)$ to denote the time-of-peak of the
epicurve restricted only to a set $S$.
The lead time for sensor set $S$ is then simply $t_{pk} - t_{pk}(S)$. 
The problem we study here is: \\
%\tozammel{the ``time of peak'' is not formally defined, and it is unclear how the
%  ``time of peak'' is calculated or measured. Since it is an important term in
%  the objective function, it would be better if the authors could give a clear
%and formal definition of the “time of peak”}

\par \noindent
\textbf{$(\epsilon, k)$-Peak Lead Time Maximization (PLTM)}
\par \noindent
\textbf{\emph{Given:}} Parameters $\epsilon$ and $k$, network $G$, and the epidemic model
\par \noindent
\textbf{\emph{Find:}} A set $S$ of nodes such that  
\begin{eqnarray*}
& S = \argmax_S  E[t_{pk}-t_{pk}(S)] \\
& \mbox{s.t.~~~}  f(S)\geq\epsilon, \\
&|S|=k
\end{eqnarray*}

Note that we  need the $f(S)$ constraint so that we only choose sets which have
a minimum probability of capturing the epidemic---intuitively, there maybe
nodes which may get infected only infrequently, but their time to infection
condition on the fact they get infected might be small (such nodes are clearly
not good `sensors').


\section{Our Approach}
Unfortunately, the peak of an epicurve is a high variance measure, making it
challenging to address directly. Further,
the expected lead time, $E[t_{pk}-t_{pk}(S)]$ is not non-decreasing and
non-submodular, in general. %\reminder{FROM ADITYA: not sure if we can give a
%quick example of why it is not submodular}
%It is easy to verify that selecting $S$ to consist of a 
%single node with the minimum expected infection time maximizes this function, while
%$S=V$ minimizes it, 
%(in contrast to the montone submodular objectives used in~\cite{Leskovec@KDD07}) \reminder{FROM ADITYA: can you please explain a bit more why it is non-monotone, maybe a proof sketch of an easy lemma?}.
Hence we consider a different, but related problem, as an intermediate step. 
Let $t_{inf}(v)$ denote the expected infection time for node $v$, given that the
epidemic starts at a random initial node. Then: \\

\par \noindent
\textbf{$(\epsilon, k)$-Minimum Average Infection Time (\textsc{MAIT})}
\par \noindent
\textbf{\emph{Given:}} Parameters $\epsilon$ and $k$, network $G$, and the epidemic model
\par \noindent
\textbf{\emph{Find:}} A set $S$ of nodes such that  
\begin{eqnarray*}
& S = \argmin_S  \sum_{v\in S} t_{inf}(v)/|S| \\
& \mbox{s.t.~~~}  f(S)\geq\epsilon, \\
&|S|=k
\end{eqnarray*}

\par \noindent
\textbf{Justification:} In contrast to the peak, note that the \emph{integral}
of the epicurve restricted to $S$, normalized by $|S|$, corresponds to the
\emph{average infection time} of nodes in $S$, which is another useful metric
for characterizing the epidemic. Further, if the epicurve has a sharp peak,
which happens in most real networks, and for most disease parameters, the
average infection time is likely to be close to $t_{pk}$. 

%\noindent
\subsection{Approximating MAIT}
The MAIT problem involves $f(S)$, which can be seen to be submodular, following
the same arguments as in \cite{Kempe03Maximizing}, and can be maximized using a
greedy approach.  However, the average infection time constraint $\sum_{v\in S}
t_{inf}(v)/|S|$ is non-linear as we keep adding nodes to $S$, which makes this problem challenging, and the
standard greedy approaches for maximizing submodular functions, and their
extensions~\cite{Krause@ICML08} do not work directly.
%\tozammel{Since |S| is constant, it is unclear why the objective function is non­linear.
%Is that possible to redefine it as a linear function, so that we can directly
%use the greedy algorithm as in \cite{Kempe03Maximizing}?}  
In particular, we note
that selecting a sensor set $S$ which minimizes $\sum_{v\in S} t_{inf}(v)$
(with $f(S)\geq\epsilon$) might not be a good solution, since it might have a
high average infection time $\sum_{v\in S} t_{inf}(v)/|S|$.  We discuss below
an approximation algorithm for this problem.  For graph $G=(V,E)$, let $m=|E|$,
$n=|V|$.

\begin{lemma}
\label{lemma:mait}
\iffalse
The $(\epsilon,k)$-\textsc{MAIT} problem on a graph $G=(V,E)$ can be approximated within
a factor of $O(\log{n})$ within time $O(m(\log{n})^{\log{D}})$.
\fi
It is possible to obtain a bi-criteria approximation $S\subseteq V$ for any instance of the
$(\epsilon,k)$-\textsc{MAIT} problem on a graph $G=(V,E)$, given the $t_{inf}(\cdot)$
values for all nodes as input, such that 
$\sum_{v\in S} t_{inf}(v)$ is within a factor of two of the optimum, and
$f(S)\geq c\cdot\epsilon$, for a constant $c$. 
The algorithm involves $O(n^2\log{n})$ evaluations of the function $f(\cdot)$.
\end{lemma}
\begin{proof}(Sketch)
Let $t_{inf}(v)$ denote the expected infection time of $v\in V$, assuming the disease
starts at a random initial node. Let $B_{opt}$ be the average infection time value for the
optimum; we can ``guess'' an estimate $B'$ for this quantity within a factor of $1+\delta$,
by trying out powers of $(1+\delta)^i$, for $i\leq\log{n}$, for any $\delta>0$,
since $B_{opt}\leq n$. We run $O(\log{n})$ ``phases'' for each choice of $B'$.

Within each phase,
we now consider the submodular function maximization problem to maximize
$f(S)$, with two linear constraints:
the first is $\sum t_{inf}(v)x(v)\leq B'k$ and $\sum_v x(v)\leq k$, where
$x(\cdot)$ denotes the characteristic vector of $S$. Using the result of
Azar et al. \cite{azar:icalp12}, we get a set $S$ such that $f(S)\geq c\mu(B')$,
for a constant $c$, and $\sum_{v\in S} t_{inf}(v)\leq B'k$ and $|S|\leq k$, where
$\mu(B')$ denotes the optimum solution corresponding to the choice of $B'$ for this problem.
If we have $|S|<k$, we add to it $k-|S|$ nodes with the minimum $t_{inf}(\cdot)$
values, which are not already in $S$, so that its size becomes $k$.
Note that for the new set $S$, we have $\sum_{v\in S} t_{inf}(v)\leq 2B'k$, since
the sum of the infection times of the nodes added to $S$ is at most $B'k$.

Note that the resulting set $S$ corresponds to one ``guess'' of $B'$. We take the
smallest value of $B'$, which ensures $f(S)\geq c\epsilon$. It follows that
for this solution $S$, we have $\sum_{v\in S} t_{inf}(v)/|S|\leq 2B_{opt}$ and $|S|=k$.
The algorithm of Azar et al. \cite{azar:icalp12} involves a greedy choice of
a node each time; each such choice involves the evaluation of $f(S')$ for some set $S'$,
leading to $O(n^2)$ evaluations of the function $f(\cdot)$; since  there are
$O(\log{n})$ phases, the lemma follows.
\iffalse
For a parameter $\delta\in(0,1)$, we consider 
intervals $T_i=[(1+\delta)^i, (1+\delta)^{i+1}]$, $i=0,1,\ldots,\log{D}$, and bin
the infection times in these intervals. Let $V_i$ denote the subset of nodes in $V$
whose expected infection times are in the interval $T_i$.
Consider an optimum solution $S^*$. Let $S^*_i=S^*\cap V_i$.
Let $n_i=|S^*_i|$, and let $\epsilon_i$ denote the probability that some node in
$S^*_i$ is infected, starting at a random initial infection; then, $\epsilon=\sum_i \epsilon_i$.
We ``guess'' $n'_i$ within a $(1\pm\delta)$ factor of $n_i$; this can be done by
trying all the possible $O(\log{n})$ choices for $n'_i$. Similarly, let $\epsilon'_i$
be an estimate of $\epsilon_i$, within  a $(1\pm\delta)$ factor; there are $O(\log{n})$
such choices.

For each $i=0,\ldots,\log{D}$, we consider the rank constrained submodular maximization
problem restricted to nodes in $V_i$, and use the greedy algorithm to find a solution
$S'_i$ of size $|S'_i|=O(n'_i\log{n})$ such that the probability the epidemic hits
$S'_i$ is at least $\epsilon'_i$. Then, the average infection time for $S'=\cup_i S'_i$
is within an $O(\log{n})$ factor of the optimal. The
lemma follows because there are $O((\log{n})^{\log{D}})$ problems.
\fi
\end{proof}

%We note that for the SI model, the MAIT problem can be solved 

%\noindent
%\reminder{FROM ADITYA: can we add some more reasons why this is not a practical algorithm?}


\subsection{Heuristics}
\label{sec:heuristics}

Though Lemma \ref{lemma:mait} runs in polynomial time, it is quite impractical
for the kinds of graphs we study in this paper because of the need for
super-quadratic number of evaluations of $f(\cdot)$.  Therefore, we consider
faster heuristics for selecting sensor sets.  The analysis of Lemma
\ref{lemma:mait} suggests the following significantly faster greedy approach:
pick nodes in non-decreasing $t_{inf}(\cdot)$ order till the resulting set $S$
has $f(S)\geq\epsilon$.  In general, this approach might not give good
approximation guarantees. However, when the network has ``hubs'', it seems
quite likely that the greedy approach will work well.  However, even this
approach requires repeated evaluation of $f(S)$, and can be quite slow.  The
class of social networks we study have the following property: nodes $v$ which
have low $t_{inf}(v)$ are usually hubs and have relatively high probability of
becoming infected. This motivates the following simpler and much faster
heuristic, referred to as the \textbf{Transmission tree based sensors}
heuristic:
\begin{enumerate*}
\item generate a set $\mathcal{T}=\{T_1,\ldots,T_N\}$ of dendrograms
\item for each node $v$, compute $d_{v}^{i}$, which is its depth in $T_i$, for all $i$,
if $v$ gets infected in $T_i$
\item compute $t_{inf}(v)$ as the average of the $d_{v}^{i}$, over all the
dendograms $T_i$, in which it gets infected
\item discard nodes $v$ with $t_{inf}(v)<\epsilon_0$, where $\epsilon_0$
is a parameter for the algorithm
\item order the remaining nodes $v_1,\ldots,v_{n'}$ in
non-decreasing $t_{inf}(\cdot)$ order (i.e., 
$t_{inf}(v_1)\leq t_{inf}(v_2)\leq\ldots\leq t_{inf}(v_{n'})$) 
\item Let $S=\{v_1,\ldots,v_k\}$
\end{enumerate*} 

\iffalse
In general, this approach might not give good approximation guarantees. However, when
the network has ``hubs'' which get infected early with reasonable probability, it
seems quite likely that the greedy approach will work well. 
\fi

We also use a faster approach based on dominator trees,
which is motivated by the same ideas as
greedy (referred to as the \textbf{Dominator tree based sensors} heuristic): 
\begin{enumerate*}
\item
generate dominator trees corresponding to each dendrogram
\item
compute the average depth of each node $v$ in the dominator trees (as in the transmission
tree heuristic)
\item
  discard nodes whose average depth is smaller than $\epsilon_0$
\item
we order nodes based on their average depth in the dominator tree, and
pick $S$ to be the set of the first $k$ nodes.
\end{enumerate*}

As we show in the experimental sections, we find that this heuristic performs
very well in practice.  Formally, the dominator relationship is defined as
follows.  A node $x$ dominates a node $y$ in a directed graph iff all paths from
a designated start node to
node $y$ must pass through node $x$.  In our case, the
start node indicates the source of the infection or disease.
Consider Fig.~\ref{fig:domtree} (left), a schematic of a social
contact network; all
paths from node A (the designated start node)
to node H must pass through node B, therefore B dominates H.  
Note that a person can be dominated by many other people.
For instance, both C and F dominate J, and C dominates F.
A node $x$
is said to be the unique immediate dominator of $y$ iff $x$ dominates $y$ and
there does not exist a node $z$ such that $x$ dominates $z$ and $z$ dominates
$y$.  Note that a node can have at most one immediate dominator, but may be the
immediate dominator of any number of nodes. The dominator tree $D = (V^D,E^D)$
is a tree induced from the original directed graph $G = (V^G,E^G)$, where
${V^D} = {V^G}$, but an edge $(u \rightarrow v) \in E^D$ iff $u$ is the
immediate dominator of $v$ in $G$. Figure ~\ref{fig:domtree} (right)
shows an example dominator tree.

The computation of dominators is a well studied topic and we adopt the
Lengauer-Tarjan algorithm ~\cite{LengTarjan} from the Boost graph library
implementation.  This algorithm runs in $O((|V|+|E|) \log (|V|+|E|))$ time,
where $|V|$ is the number of vertices and $|E|$ is the number of edges.

\begin{figure}[tb]
\centering
\includegraphics[width=\columnwidth]{figs/ex_domtree}
\caption{(i) An example graph and (ii) its dominator tree. In practice, the dominator
will have a significantly reduced number of edges than the original graph.
}
\label{fig:domtree}
\end{figure}


\iffalse
%Commented by Anil
%%%%%%%%%%%%%%%%%%%%
% by Huijuan
Given a social network graph $G=<V,E>$,
where $V$ denotes all the persons and $E$ denotes
the contact between any two persons.
Given a disease model spreading on this
social network $G$ with the time of day.

Three goals are targeted.
1. Select some people $S_{sesnor}$,
which can predict the disease peak $T_{sp}$ of $S_{sesnor}$
2. Predict the disease $T_{wp}$ of whole population $S_{whole}$.
3. The predicted peak is assumed to be as accurate as possible.
4. The predicted peak is assumed to be as early as possible.
4. The sensor size is assumed to be as small as possible.

\newtheorem{hjDef}{Definition}
\begin{hjDef}
Given a undirected graph $G=<V,E>$, which represetns the whole social network,
the vertex set $V$ represents the set of persons in this network.
In addition, several attributes are associated with each person $v \in V$, such as age, gender, income, etc..
Edges in $G$ denotes certain interaction between any two persons connected by an edge.
An edge $e \in E$ also has attributes of meeting location
and meeting duration.
% We track the infected status of each person over a period of time $T$.
Some disease may be spread over this social network. However, monitoring the whole population to track the outbreak of this disease is impractical in large social networks. So, we want to monitor a group of persons, in which the outbreak of the disease leads that of the whole population for a certain time. Such group of persons can be called as social network sensors.
The problem is to select $S \subset V$ as social network sensors to predict the disease outbreak time $T_{W}$ in the social network through monitoring the disease outbreak time $T_{S}$ of social network sensors $S$. We also want to maximize the time difference of two outbreaks $\Delta T = T_{W} - T_{S}$, and at the same time, keep a relative high prediction accuracy (for example, the predicted outbreak time $T_{W}$ should be in the 95\% confidence interval of the true disease outbreak time, which could be estimated by historical data or simulated disease spread experiments).

%\begin{displaymath}
%\forall S^\prime \subset V ,
%T^\prime_{pre} = T_{W} - T_{S}
%\end{displaymath}
%\begin{displaymath}
%T_{pre}^\prime  \leq  T_{pre}
%\end{displaymath}
%subject to
%\begin{displaymath}
% CI(T_{pre})>=0.95,  CI(T_{pre}^\prime)>=0.95,
%\end{displaymath}

\end{hjDef}

==============
%By Maleq

We are given a social contact network $G=(V,E)$, where each node represents a person and there is an edge between two persons if they are in contact with each other for a certain duration. Two persons come in contact when they perform some activity together (i.e., when they visit the same location for some overlapping time interval). Type of a contact is the type of the corresponding activity. In this contact network, both nodes and edges are labeled. The node labels are the demographic information of the corresponding person, and the edge labels are contact duration and contact type. Example of the labels associated to a node are age, income, gender, home location, household size, etc. (For edge labels, in addition to contact duration and type, contact location may also be available). There can be parallel edges (two or more edges) between two nodes, if these two individuals meet multiple times at disjoint time intervals. However, for many practical purposes, such parallel edges can be collapsed into a single edge. If there are two edges between nodes $u$ and $v$ with contact durations $t_1$ and $t_2$, we can replace these two edges by a single edge with contact duration $t_1+t_2$. We assume that the parallel edges have been combined into single edges, i.e., the given contact network $G$ is simple.

\emph{Vulnerability} of a node (a person) is defined to be the probability that this node becomes infected when an epidemic begins at random initial infected nodes. Let $\beta(v)$ denotes the vulnerability of node $v \in V$.  Vulnerability of a node can be estimated by simulating a disease many times using the same disease parameters (parameter characterizing the disease such as incubation period, infectious period, transmissibility -- how easily the disease transmit from one to another, etc.) but picking different random initial infections for each simulation. If a given node $v$ gets infected $s'$ times out of $s$ simulation runs, the estimated vulnerability $\beta(v) = \frac{s'}{s}$. It can be computationally challenging to simulate an epidemic disease that many times to get an accurate estimate of vulnerability. We use a fast simulation for disease dynamics, EpiFast \cite{epifast} developed at NDSSL.

A \emph{dendrogram} $D=(V_D, E_D)$ is a labeled graph used to represent the stochastic output of an epidemic process on a network $G = (V,E)$. The nodes $v \in V_D$ in a dendrogram $D$ are the nodes in $G$ that are infected; we have  $V_D \subseteq V$. The edge set $E_D \subseteq V$ is a set of edges that are used to spread the infections. The day on which infection is spread via an edge is a label of the edge.

\emph{Time-vulnerability}: Time-vulnerability of a node measures how quickly a node gets infected. Let $t(v)$ be the time when node $v$ gets infected (we need to define $t(v)$ for uninfected nodes).  Now time-vulnerability can be defined in the following two ways:
\begin{itemize}
\item Average of $t(v)$ over multiple runs of simulation.
\item Probability that $t(v)$ is in $[0, T]$ for a given $T$.
\end{itemize}

\emph{Depth-vulnerability}: Depth $\delta(v)$ of a node $v$ in a dendrogram is the length of the shortest path from the source of the infection to $v$. Now depth-vulnerability can be defined in the following two ways:
\begin{itemize}
\item Average depth of $v$ over multiple dendrograms from multiple runs of simulation. (In this case, we need to define depth of an uninfected node.)
\item Distribution of $\delta(v)$.
\end{itemize}

==============

Q. What is vulnerability?
Given a set of initial conditions, vulnerability of a node is the probability of getting infected

Q. What is time-vulnerability?

Time vulnerability of a node is the probability of getting infected within a time window

Q. What is depth-vulnerability?

Look at all transmission trees. Measure depth of  every node. Average depth will give us a depth vulnerability.
It will also give us some sort of time vulnerability.

Definition of sensors:

Over all possible choice of initial conditions, a set of K nodes is good sensor if it has following properties:

1. Infection happens early enough (low depth in transmission tree)

2. If certain amount of infection occurs in the sensor group, then an epidemic curve is going to take off in the near future

3. once they are infected then prediction quality is better than any other K nodes

From the transmission trees:

	Sensor-value(u) = E[d(u)]  (over all trees) [For a single node]
	Sensors-value(S) = which minimize the sum of depth

Other qualities we want from a sensor: to be able to forecast:
value of the peak, time of the peak, cumulative of the infected
%Anil: end of comment
\fi
