\section{Related Work}
\label{sec:related}

There are several existing literatures on detecting outbreaks in networks.
Christakis and Fowler~\cite{christakis:10:sensor} proposed a simple heuristic
that monitors the friends of randomly chosen individuals from a social network
as sensors to achieve early detection of epidemics. However, they only
demonstrated their proposed approach on a relatively small social network, e.g.\
student network from Harvard College. As we have shown earlier, their friend
heuristic fails on large social contact networks of US cities. Leskovec et.\
al.~\cite{Leskovec@KDD07} defined objective functions with submodularity to pick
optimal locations to place sensors in water and blog networks, subject to
several metrics like population affected, and time-to-first-detection. In
contrast, our metrics are \emph{not} submodular, more complex (shifts in peak
time) and more realistic for biological epidemics, giving significant additional
time for reaction.

There are a lot of research interests in studying different types of
information dissemination processes on large graphs, including (a)
information cascades~\cite{Bikchandani:1992,Goldenberg:2001}, (b) blog
propagation~\cite{Leskovec:2007:sdma,Gruhl:2004,Kumar:2003,Richardson:2002}, and
(c) viral marketing and product penetration~\cite{Leskovec:2006:ec}. The
classical texts on epidemic models and analysis are May and
Anderson~\cite{andersonmay} and Hethcote~\cite{hethcote2000}. Widely-studied
epidemiological models include {\em homogeneous
models}~\cite{Bailey1975Diseases,McKendrick1926Medical,andersonmay} which assume
that every individual has equal contact with others in the population. Much
research in virus propagation studied the so-called epidemic threshold on
different types of graphs, that is, to determine the condition under which an
epidemic will not break
out~\cite{kephart1993,vespignani2001,deepay2008,ganesh05effect,Prakash@ICDM11}.

Detection and forecasting are fundamental and recurring problems in public
health policy planning, e.g.,~\cite{nishiura11, mckinley09, nsubuga06,
nsoesie11}. National and international public health agencies are actively
involved in syndromic surveillance activities to detect outbreaks of different
infectious diseases---such surveillance information could include confirmed
reports of infections, and estimates of the number of infections. In the initial
days of an outbreak, such information is very limited and noisy, and
understanding the true extent of the outbreak and its dynamics are challenging
problems, e.g.,~\cite{shmueli10}. As in the case of the swine flu pandemic a few
years back\cite{nsubuga06}, whether the epidemic has peaked, is a fundamental
problem. Some of the few papers \cite{nsoesie11, mckinley09} consider the
problems of estimating the temporal characteristics of an outbreak. They
use simulation based approaches for model based reasoning about epicurves and
other characteristics.

Another related problem is immunization, i.e.\ the problem of finding the best
vertices for removal to stop an epidemic, with effective immunization strategies
for static and dynamic graphs~\cite{Hayashi03Recoverable, Tong@ICDM10,
Briesemeister03Epidemic\hide{, prakash2010}}. Other such problems where we
wish to select a subset of `{\em important}' vertices from graphs, include
`finding most-likely culprits of epidemics'~\cite{lappas:10:effectors,
Prakash@ICDM12} and the influence maximization problem for viral
marketing~\cite{richardson2002mining, chen2009efficient,kimura2006tractable}. 
