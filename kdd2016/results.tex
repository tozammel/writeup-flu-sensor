\begin{figure*}[!t]
	\vspace{-0.15in}
	\begin{minipage}{0.495\textwidth}
		\centering
		\includegraphics[width=1.7in]{figs/Fig4a}
		\includegraphics[width=1.7in]{figs/Fig4}
		\caption{\textbf{Daily incidence of sensor sets selected by the heuristic
			approaches compared to the true daily incidence in the simulated
			epidemic on (a) \oregon~dataset (left), (b) \miami~dataset (right).}}
		\label{fig:shift-vs-size}
		\vspace{0.7cm}
	\end{minipage}
	\hfill
	\begin{minipage}{0.495\textwidth}
		\centering
		\includegraphics[width=1.7in]{figs/Fig5a}
		\includegraphics[width=1.7in]{figs/Fig5}
		\caption{\textbf{The expected peak time of the daily incidence curve on
		(a) \oregon~dataset (left), (b) \miami~dataset (right).} Here Top-3, WD,
		TT, and DT denote Top-3 high degree, Top-3 weighted degree, Transmission
		tree based, and Dominator tree based heuristic respectively. }
		\label{fig_dailyInc4}
	\end{minipage}
\end{figure*}

\section{Experimental Results}
\label{sec:exp}
Our experimental investigations focus on addressing the following questions:
\begin{enumerate*}
	\item How do the proposed approaches perform when forecasting the epidemic
		in terms of the lead time? (Section~\ref{infection-time})
	%\item For a given sensor network size, how well do the algorithms perform in
		%terms of the PLTM problem (the peak lead time problem)?
		%(Section~\ref{lead-time}) 
	\item How large should our sensor set size be? (Section~\ref{sensor-size})
	%\item How does lead time vary as a function of disease parameters?
		%(Section~\ref{disease-param})
  	\item How many days are necessary to observe a stable lead time?
	  (Section~\ref{stability})
	\item What is the predictive power of the sensor set in estimating the
        epidemic curve over the full population? (Section~\ref{predict-epicurve})
  	\item Is it possible to employ surrogates for sensors? (Section~\ref{surrogates})
\end{enumerate*}

Table~\ref{tab:dataset} shows some basic network statistics of the datasets we
used in our experiments. The Oregon AS (Autonomous System) router graph is an
AS-level connectivity network inferred from Oregon
route-views~\cite{oregondata}. Although this dataset does not relate to
epidemiological modeling, we use it primarily as a testbed to understand how
(and if) graph topology affects our results due to the relatively small size and
neat graph structure; the rest of the datasets are generated with specific aim
at modeling epidemics in human populations.  These datasets are synthetic but
realistic social contact networks for six large cities in the United States.
Here, we briefly describe the major steps to generate these synthetic datasets
(see~\cite{barrett:wsc09,nature} for details): (i) a synthetic urban population
model is constructed by integrating a variety of public (e.g., US Census) and
commercial data (Dunn \& BradStreet), which is statistically equivalent to the
real population; (ii) activity sequences are constructed for each household and
each person by matching activity surveys. The activity data also specify the
type of each activity and the duration of performing it; (iii) activity
locations are assigned for each person, using land use data and activity choice
models; (iv) individuals are routed through the road network, which gives a
social contact network based on location co-occurrences.

\begin{table}[!t]
	\centering\vspace{-0.3in}
	\caption{\textbf{Statistics of datasets used in the experiments.}}
	\label{tab:dataset}
	\begin{tabular}{|l|c|c|c|}
	  \hline
	  Dataset  & Nodes     & Avg.\ deg &  Max deg\\
	  \hline
	  \oregon  & 10,670    & 4.12     &  2,312\\
	  \hline
	  \miami   & 2,092,147 & 50.38     &  425 \\
	  \hline
	  %\seattle & 3,207,037 & 55.35     &  456\\
	  %\hline
	  \boston & 4,149,279 & 108.32 & 437 \\
	   \hline
	  \dallas & 5,098,598 & 113.10  & 477 \\
	   \hline
	  \chicago & 9,047,574 & 118.83  & 507 \\
	   \hline
	  \losangeles & 16,244,426 & 113.08 & 463 \\
	   \hline
	  \newyork & 20,618,488 & 93.14 & 464 \\
	  \hline
	\end{tabular}
\end{table}

In our experimental study, we evaluated our two proposed approaches,
transmission tree based heuristic and dominator tree based heuristic. For
comparison, we also implemented two strategies as baseline methods: (i)
\textbf{Top-K high degree sensors} heuristic used in~\cite{christakis:10:sensor}
where a set $P \subseteq V$ is first sampled and for each $v \in P$ its $K$
neighbors with largest degree are selected and (ii) \textbf{Weighted degree (WD)
sensors} heuristic, which is similar to the previous heuristic except that the
$K$ neighbors are chosen based on largest weighted degree. The weight we use
here is the durations of the activities indicated by edges of the graphs in the
datasets mentioned in Table~\ref{tab:dataset}. However, since we don't have
these weights for the \oregon~dataset, we will omit the results of the WD
sensor heuristic on the \oregon~dataset.

Our primary figure of merit is the lead time, calculated as follows. For each
run of the disease model in a social contact network, we fit a logistic function
curve to the cumulative incidence of the chosen sensor set and a random
sampled set from $V$. Here, we use the random sampled set to represent the
entire population since for such large city-level datasets we used in our
experiments, it is usually impossible to track the entire population in
practice. We then derive daily incidence curves for both the sensor
set and the random set (we will refer this set as random set in the rest of this
paper). Let $t_s$ and $t_r$ represent the peak times of the daily incidence
curves for the sensor and random sets respectively, and the lead time is defined
as $\Delta t = t_r - t_s$. For all the experiments in this section, the
parameters for the epidemic simulations are set as follows unless specified. We
set $\epsilon = 0.8$ (see the definitions of the PLTM and MAIT problems) and flu
transmission rate to be $4.2 \times 10^{-5}$ for the SEIR disease model. The
size for the sensor set and random set ($k$) is $5\%$ of the entire population,
and the epidemic simulations start with five randomly infected vertices in the
networks. All the results were obtained by averaging across $1,000$ independent
runs.

\begin{table*}[!t]
	\centering
	\caption {\textbf{Comparison of the lead time across four different social
		sensor selection heuristics when the number of initial infected vertices
		vary.}}
	\label{tab_bcmp}
	\begin{tabular} {|l|c|c|c|c|c|c|c|c|}
		\hline
		\multirow{2}{*}{Dataset} & \multirow{2}{*}{Seed} & \multicolumn{4}{c|}{Lead time} \\  \cline{3-6} 
		&  & Top-K degree & Weight degree  & Transmission tree & Dominator tree
		\\ \hline
		%\oregon &1& 7.84  & n/a  & 0.17  & 2.21 \\ \hline
		%& 5 & 7.96 & n/a  & 0.40 & 1.45 \\ \hline
		%& 10 & 8.01 & n/a & 0.59 & 1.07\\
		\multirow{3}{*}{\oregon} & 1 & 13.13 & n/a  & 10.10  & 9.91 \\ \cline{2-6}
		& 5 & 8.85 & n/a  & 7.93 & 7.75 \\ \cline{2-6}
		& 10 & 11.00 & n/a & 8.63 & 8.55 \\
		\hline
		\multirow{3}{*}{\miami} & 1 & 0.29  & 3.38  & 10.46  & 10.08 \\ \cline{2-6}
		& 5 & 0.39 & 3.41  & 10.15  & 10.19 \\ \cline{2-6}
		& 10 & 0.62 & 3.41 & 10.13 & 10.13\\
		\hline
		%\seattle
		%& 1 & -0.19   & 4.01  & 8.96  & 8.91 \\ \hline
		%& 5 & -0.15 & 3.98 & 8.90  & 8.84 \\ \hline
		%& 10 & -0.14 & 3.64 & 8.92 & 8.84\\
		%\hline
	\end{tabular}
\end{table*}

% Todo: modify the following text to incorporate the results on Oregon dataset.
\subsection{Performance of the predicted epidemic lead time}
\label{infection-time}
%\hao{A lot of issues for the results. 1. what is the parameter settings for the
%disease simulation? 2. Why is there only results on \miami{} dataset? What about
%the results on the other two dataset? 3. How many independent runs did you
%perform to get these results?}

In this section, we study how our proposed heuristic approaches performs in
terms of the predicted epidemic lead time. We apply the two proposed approaches,
transmission tree (TT) and dominator tree (DT) based heuristics, and two base
line approach, Top-K high degree (Top-3 in our experiments) and weighted degree
(WD) based heuristics, to the \oregon~and \miami~datasets. As shown in
Table~\ref{tab:dataset}, \oregon~dataset clearly has a different network
topology structure compared to \miami~dataset, and here we use \oregon~dataset
to verify whether our proposed heuristics are robust to different network
topologies. In this experimental study, we set the flu transmission rate to
$0.05$ for the SEIR model in the \oregon~dataset due to its relatively
small size compared to the \miami~dataset. Fig.~\ref{fig:shift-vs-size} depicts
the daily incidence curves of the four sensor selection heuristics and the
random set on \oregon~and \miami~datasets, and Fig.~\ref{fig_dailyInc4}
describes the corresponding peak time of the daily incidence curves shown in
Fig.~\ref{fig:shift-vs-size}. As we can see from these figures, on
\oregon~dataset, the performance of the proposed heuristics and baseline
heuristics is comparable where they both predict the peak of the epicurves about
five days earlier when compared to the ground truth. However, on the
\miami~dataset, the proposed TT and DT heuristic approaches give a much larger
lead time, around 10 days, compared to the about two-day and almost zero day
lead time in the WD and Top-K baseline heuristics. This is because, as described
earlier, our approaches are precisely designed to try to pick vertices with
early expected infection time from the disease propagation network as social
sensors. We also study whether the number of the initial infected vertices will
affect the predicted lead time.  Table~\ref{tab_bcmp} shows the predicted lead
time of the two proposed and the two baseline heuristics for 1, 5 and 10 initial
infected vertices in the epidemic simulations. As the results in this table
shows, the number of initial infected vertices would not have too much impact on
the predicted lead time.

To explain why the proposed social sensor selection heuristics work better, we
start from analyzing the structures of the disease propagation networks.
Comparing the graph statistics of \oregon~dataset with \miami~dataset shown in
Table~\ref{tab:dataset}, we can observe that the graph in the \oregon~dataset
has a quite different topology structure from the graphs in the \miami~datasets.
The graph in the \oregon~dataset has relatively small average degree but very
large maximum degree, which indicates this graph has star-like topology where
few of the central vertices have very large degrees. On the other hand, many
vertices in the graphs of the \miami~datasets have large degrees, and they
spread all over the entire graph. Thus, for the top-K degree based sensor
selection approach, it is relatively easy to include the central vertices
with high degrees into the sensor set in \oregon~dataset, but for the
transmission tree and dominator tree based approaches, whether the high degree
vertices are included into the sensor set will heavily depend on the choices of
initial seeds of the epidemics in the \oregon~network. Such central vertices
with high degree are usually very important for the epidemics in such star-like
networks, which explains why the top-K degree approach works better than the
transmission tree and dominator tree approaches. On the contrary, in the
\miami~dataset, the total number of vertices is large, and it is quite difficult
for top-K degree approach to select sensors that could represent the entire
graph only based on the local friend-friend information. However, the
transmission tree and dominator tree based sensor selection strategies take the
global epidemic spread information into account, which chooses the sensor set
that could represent the entire graph. That's why they perform better in term of
the lead time than the top-K degree based approach on the large simulated US
city networks. The results of this experiment further demonstrate that the
network topology must be considered when designing social sensor selection
strategies. They also show that the proposed TT and DT based sensor selection
heuristics are more robust to the underlying network topologies, and thus more
suitable to be deployed in practice, such as monitoring and forecasting
epidemics in large cities.

\begin{figure*}[!t]
	\begin{minipage}{0.66\textwidth}
		\centering
		\includegraphics[width=2.2in]{figs/Fig6a}
		\includegraphics[width=2.2in]{figs/Fig6b.png}
		\caption{\textbf{Mean lead time (left) and inverse of variance-to-mean
			ratio (right) v.s.\ the sensor size for the \miami~dataset.} When
			sensor set size is less than $1.0\%$ of the entire population we
			observe higher (good) lead time, but also with high variances.
			Scaling the mean lead time by the variance, i.e., the reciprocal of
			the Fano factor, shows a clear peak with the sensor set size at
			approximately $20\%$ of the population, the position where we can
			obtain substantial gains in lead time with correspondingly low
			variances.}
		\label{size_miami}
	\end{minipage}
	%\hfill
	%\begin{minipage}{0.245\textwidth}
		%\includegraphics[width=1.7in]{figs/Fig8b}
		%\caption{\textbf{Lead time v.s.\ disease transmission rate on
			%\miami~datasets.} The size of sensor set and random set is fixed at
			%5\% of the entire population. Notice that lines for dominator tree
			%and transmission tree based heuristics are overlap with each other.}
		\vspace{0.3cm}
		%\label{fig_parameter}
	%\end{minipage}
	\hfill
	\begin{minipage}{0.33\textwidth}
		\includegraphics[width=2.2in]{figs/Fig9a}
		\caption{\textbf{Stability of the lead time estimation. The etimated the
		lead time fluctuates initially. As the number of monitoring days
		increases, it stabilizes quickly.}}
		\vspace{0.6cm}
		\label{fig_fitPredict}
	\end{minipage}
\end{figure*}

\subsection{How many sensors to choose?}
\label{sensor-size}
Since we have already demonstrated the influences of the network topology on
social sensor selection strategies, we will put the \oregon~dataset aside, and
focus on the social contact network datasets for US cities in the rest of the
experiments. An interesting conundrum is the number of sensors to select in a
design. Fig.~\ref{size_miami} depicts the mean lead time and the inverse of
variance-to-mean ratio of the lead time v.s.\ the sensor size for the
\miami~datasets. The results show that the variance of the lead time estimate is
high for small size of sensor sets and decreases as the sensor set size
increases. This suggests a natural strategy of scaling the lead time against the
variance, thus helps establish a sweet spot in the trade-off.  This
variance-to-mean ratio is also known as the {\it Fano factor}, which is widely
used as an index of dispersion. In the result for \miami~dataset, there is a
clear peak in the figure of the inverse of variance-to-mean ratio, which
suggests a suitable size of sensors to pick.

%\subsection{Effect of the disease transmission rate on the lead time}
%\label{disease-param}
%In this experiment, we measure the influences of the disease transmission
%parameter to the predicted epidemic lead time. We change the disease
%transmission rate from $3.5 \times 10^{-4}$ to $5.5 \times 10^{-4}$.
%Fig.~\ref{fig_parameter} describes the predicted lead time on the \miami~dataset
%for the four sensor selection heuristics under consideration. As expected, our
%proposed transmission tree and dominator tree based heuristics consistently
%outperform the two baseline heuristics as the disease transmission rate changes.
%Also, We observe a stable estimated lead time when the disease transmission rate
%varies, which demonstrate that the disease transmission rate will not influence
%too much on the predicted lead time.

\subsection{Empirical study on stability of lead time}
\label{stability}
In this experiment, we study the stability of the estimated lead time as we
observe more data on the sensor group when the number of monitoring days
increases. As is well known, the cumulative incidence curve of flu epidemics can
be modeled by logistic function where the dependent and independent
variables are the flu cumulative incidence and the time of the epidemic (days in
our context). Here, we vary our flu epidemic simulation time from 2 days to
300 days on the \miami~dataset, estimate cumulative incidence curves (with
logistic function) for both the sensor and the random set based on the
simulated cumulative flu incidence data, and then compute the lead time.
Fig.~\ref{fig_fitPredict} shows the lead time v.s.\ the flu epidemic simulation
time. As we can see from this figure, the estimated lead time fluctuates a lot
when the simulation time is short and stabilizes at around 12 days when the
epidemic simulation time is more than around 80 days. Such results provide some
insights for public health officials on how much epidemic data they should
collect in order to make an accurate estimation of the flu outbreak from the
time domain perspective.

\subsection{Predicting population epidemic curve from sensor group epidemic
curve} 
\label{predict-epicurve}

In this experiment, we study the relationship between the flu cumulative
incidence curve of sensor and that of random group. As we mentioned before, we
use random set to represent the entire population since it is usually quite
difficult to characterize the entire population in practice when the dataset is
quite large. We try to estimate a polynomial regression model with degree of
three where the observed cumulative incidence of the sensor group serves as
predictor and that of the random group serves as responses. Here, the sensor
group is selected by the dominator tree heuristic from \miami~dataset. Over the
300 simulated days, we use the data of the first 150 days to estimate our
polynomial regression model, and make predictions of the cumulative incidence of
random group for the rest of the 150 days. Fig.~\ref{fig:predict_cum} shows the
fitted polynomial regression model compared to the true relation curve of the
flu cumulative incidences between sensor group and random group. As we can see
from this figure, the polynomial regression model with degree of three could
capture the relationship between the cumulative incidences of random group and
sensor group quite well, which can help us predict the epidemic curve of entire
population with epidemic data collected from the sensor group.

\begin{figure*}[!t]
	\begin{minipage}{0.33\textwidth}
		\centering
		\includegraphics[width=2.2in]{figs/Fig9b.png}%\vspace{0.3cm}
		\caption{\textbf{Predicting cumulative incidence of random group with
		sensor group for \miami~dataset.}}
		\label{fig:predict_cum}
	\end{minipage}
	\hfill
	\begin{minipage}{0.66\textwidth}
		\centering
		\vspace{-0.5cm}
		\includegraphics[width=2.2in]{figs/Fig10a}
		\includegraphics[width=2.2in]{figs/Fig10b}
		\vspace{-0.1cm}
		\caption{\textbf{Distribution of ages for sensor groups (left) and random
			groups (right).}}
		\label{figg1}
	\end{minipage}
\end{figure*}

\subsection{Surrogates for social sensors}
\label{surrogates}

\begin{figure*}[!t]
	\begin{minipage}{0.495\textwidth}
		\centering
		\includegraphics[width=1.7in]{figs/Fig11a}
		\includegraphics[width=1.7in]{figs/Fig11b}
		\caption{\textbf{Distribution of total meeting duration time with
			neighbor vertices for sensor groups (left) and random groups (right).}}
		\vspace{0.7cm}
		\label{figg2}
	\end{minipage}
	\hfill
	\begin{minipage}{0.495\textwidth}
		\centering
		\includegraphics[width=1.7in]{figs/Fig12a}
		\includegraphics[width=1.7in]{figs/Fig12b}
		\caption{\textbf{Distribution of meeting types for sensor groups (left) and
			random groups (right).} The meeting types in the datasets correspond to
			home (1), work (2), shop (3), visit (4), school (5), and other (6).  }
		\label{figg3}
	\end{minipage}
\end{figure*}

Both of our proposed approaches (TT and DT heuristics) and the previous
experiments we have conducted are based on an importation assumption that we
know the detailed structure of the social contact network. Thus, we are able to
analyze the network structure, and identify the good sensor nodes by direct
inspection. However, in reality, the structures of large scale social contact
networks are usually unknown or difficult to obtain, which makes it difficult to
directly apply our proposed methods.

In order to make the proposed approaches deployable and solve realistic public
health problems, we now relax this key assumption, and try to find a {\it
surrogate} approach to select social sensors. In this case, the policy makers
can implement their strategies without detailed (and intrusive) knowledge of
people and their activities. Surrogates are thus an approach to implement
privacy-preserving social network sensors.

The key idea of our surrogate approach is to utilize the demographic
information. Here, we use \miami{} dataset as an example to explain our
surrogate approach. We extracted the following 16 demographic features from
\miami{} dataset:
\begin{itemize}[itemsep=-3pt]
	\item Age, gender, and income
	\item Number of meetings with neighbor nodes
	\item Total meeting duration with neighbor nodes
	\item Number of meetings whose durations are longer than 20000 seconds
	\item Number of meetings of types 1--5
	\item Percent of meetings of types 1--5
\end{itemize}
The meeting types of 1--5 refer to home, work, shop, visit and school,
respectively. Among all these features, we first identify the differences of
the feature distributions between the sensor set selected by the proposed
transmission tree (or dominator tree) based heuristic and the random set when
the network structure is known. Large difference indicates that
the corresponding demographic feature characterizes the sensor set, thus could
be used to select surrogate sensors. Here for the \miami~dataset, we choose the
features of \emph{Age}, \emph{Total meeting duration with neighbor nodes} and
\emph{Meeting types} to help select surrogate sensors since these three features
best characterize the sensor set for the \miami~dataset.
Fig.~\ref{figg1},~\ref{figg2} and~\ref{figg3} compare the empirical
distributions of the sensor set and the random set for the three selected
features for \miami~dataset.
%\hao{Here, only comparing the distributions of these three features won't
%demonstrate that these three are the most important feature to use to select
%surrogates. There should be a table of KL divergences comparing all the
%demographic features appeared in the datasets. Also, change the Y-axis of the
%following three figures into relative frequency.}

Using the three identified features, we derived the following three criteria to
choose surrogate sensors:
\begin{itemize}[itemsep=-3pt]
	\item People must come from the age group of 5-20 years (Fig.~\ref{figg1}).
	\item At least $80\%$ of the meetings with the neighbor vertices must have
		durations greater than $20,000$ seconds (Fig.~\ref{figg2} ).
	\item At least $80\%$ of the meetings with the neighbor vertices must be
		type $2$ or $5$ (Fig.~\ref{figg3}).
\end{itemize}
Applying these three criteria to the entire population of the \miami~dataset, we
obtained a surrogate sensor set $S^{\prime}$ of size $211,397$, which is still
too large to monitor compared to the typical survey size, e.g.\ $2,000$, in the
public heath studies. Thus, we need more rigorous criteria to further refine the
surrogate sensors. 

Here, we apply the classification and regression tree (CART) method. It should
be pointed out that although we choose CART algorithm there, any other
supervised classification algorithm (e.g., decision trees) can also be used to
refine the surrogate social network sensors. The 16 attributes mentioned above
are used as independent variables in our CART model, and the response variable
is binary to indicate whether a person should be selected as a sensor or not. In
order to learn the CART model, we create the training data as follows. We choose
$0.1\%$ of the entire population ($\approx 2000$) in \miami{} dataset with our
proposed heuristics as the training data with positive responses (social
sensors), and choose another $0.1\%$ randomly as the training data with negative
responses (not social sensors). Then, separate CART models were learned to
refine the surrogate sensor set $S^{\prime}$ for each transmission rate ranging
from $3.0 \times 10^{-5}$ to $5.5 \times 10^{-5}$ with a step size of $5 \times
10^{-6}$. Such transmission rates are the typical values used in various flu
epidemic studies. Each of these CART models selected approximate $30,000$
individuals as surrogate sensors from $S^{\prime}$, and we choose the common
individuals across all the CART models as the final surrogate sensor set
$S^{\prime\prime}$ whose size is $17,393$.

Fig.~\ref{fig_surrogate_cart} compares the estimated lead time between the
surrogate sensor set $S^{\prime \prime}$ and the sensor set selected by
dominator tree heuristic for various flu transmission rates. As we can see from
this figure, although the surrogate sensor set $S^{\prime\prime}$ does not
perform as well as the proposed dominator tree based sensor set, it still
provide a significant lead time, which is good enough to give early warning
to the public health officials for the potential incoming flu outbreak. Most
important, since the CART based surrogate sensor approach does not require the
information of the social contact network structures, it is easy to implement
and deploy in reality compared to the transmission tree and dominator tree based
heuristic approaches. This makes it a promising candidate for predicting flu
outbreaks for public health officials.

\begin{figure*}[!t]
	\begin{minipage}{0.32\textwidth}
		\centering
		\includegraphics[width=2.2in]{figs/Fig13}
		\caption{\textbf{Mean lead times estimated with surrogate sensor set
			$S^{\prime \prime}$ and dominator tree based social sensors for
			various flu transmission rates.}}
		\label{fig_surrogate_cart}
	\end{minipage}
	\hfill
	\begin{minipage}{0.64\textwidth}
		\centering
		\includegraphics[width=2.2in]{figs/Fig14a}
		\includegraphics[width=2.2in]{figs/Fig14b}
		\caption{\textbf{The lead time of transmission tree based (left) and
			dominator tree based (right) sensor selection strategies using
			different combinations of individual demographic and interaction
			information on Miami, Boston, Dallas, Chicago, Los Angeles and New
			York City datasets.}}
		\label{fig_surrogate_lead}
	\end{minipage}
\end{figure*}

\subsection{What information should be used to select surrogate sensors?}
Notice that in the last section, when we select the surrogate sensors with CART
algorithm, both demographic (e.g.\ age of individuals) and interaction (e.g.\
total meeting duration and meeting types with neighboring individuals)
information is taken into account. However, which kind of information is more
important in term of estimating the lead time of flu epidemics? What information
should be collected first if the resources are limited for public health
officials? Such practical issues need to be considered when developing surrogate
sensor selection strategies. In this section, we study how the individual
demographic and interaction information influence the estimated flu epidemic
lead time.

In this experiment, besides the \miami~datasets we used in the previous
experiments, we include five other synthetic but realistic social contact
network datasets for large US cities, e.g.\
\boston,~\dallas,~\chicago,~\losangeles~and \newyork. For each city, we
selected the surrogate sensor set and the random set with the fixed size of
$10,000$. The sensor set were selected with the following six strategies: 1)
using empirical distributions of demographic information (distr demo); 2) using
empirical distributions of interaction information (distr inter); 3) using CART
with demographic information (CART demo); 4) using CART with interaction
information (CART inter); 5) using CART with both demographic and interaction
information (CART demo+inter); 6) using transmission tree or dominator tree
based heuristic (trans or dom). We computed the lead time for each of the six
surrogate sensor selection strategies mentioned above, and the results were
averaged across $100$ independent runs. Figure~\ref{fig_surrogate_lead} shows
the lead time of the different approaches over the six US city datasets. As we
can see from the figure, our proposed approaches (CART based approaches and
transmission/dominator tree based approaches) outperforms the two baseline
methods (distr demo/inter), and in general, as more information is taken into
account, the larger estimated lead time could be achieved (since the
transmission/dominator tree based heuristics assume known social contact network
structures, they could be thought of possessing the most information about
epidemics). Furthermore, the individual interaction information seems to be more
important than the demographic information from the perspective of obtaining
larger lead time. Such findings provide some general guidelines for public
health officials on how to design surveys to collect public data in order to
predict flu epidemics.
