\section{Introduction}
\label{sec:intro}

Given a graph and a contagion spreading on it, can we monitor some nodes to get
\emph{ahead} of the overall epidemic? This problem is of interest in multiple
settings. For example, it is an important problem for public health and
surveillance, as such sensors can provide valuable lead time to authorities to
react and implement containment policies. Similarly in a computer virus
setting, it can provide anti-virus companies lead time to develop solutions. 

Many existing methods for such detection problems typically give indicators
which lag behind the epidemic. Recent work~\cite{christakis:10:sensor} has made
some advances, using the so-called `Friend-of-Friend' approach to select such
sensors. After implementing it among the students at Harvard, Christakis and
Fowler found that the peak of the daily incidence curve (the number of new
infections per day) in the sensor set occurs 3.2 days earlier than that of a
same-sized random set of students. Intuitively, this implies that if
public-health officials monitor the sensor set, they can a get a significant
lead time before the outbreaks happen in the \emph{population-at-large}.
Unfortunately, the heuristic proposed in~\cite{christakis:10:sensor} has a few
shortcomings as we will show next. In fact, this heuristic can give \emph{no}
lead time.

\begin{figure*}[!t]
	\centering\vspace{-0.15in}
	\includegraphics[width=2.2in]{figs/Fig1a.png}
	\includegraphics[width=2.2in]{figs/Fig1b.png}
	\includegraphics[width=2.2in]{figs/Fig1c.png}
	\caption{\textbf{Illustration of the Friend-of-Friend
		approach~\cite{christakis:10:sensor} on the \oregon{} dataset.} (a) True
		daily incidence curve (left), (b) fitted daily incidence curve with
		logistic function (middle), and (c) distribution of lead time over 100
		experiments (right). Note that there is a non-zero lead time observed,
		i.e., the peak of the sensor curve occurs earlier than the peak of the
		curve for the random group.}
		\label{fig_degreeOregonSI}\vspace{-0.15in}
\end{figure*}

\begin{figure*}[!t]
	\centering
	\includegraphics[width=2.2in]{figs/Fig2a}
	\includegraphics[width=2.2in]{figs/Fig2b}
	\includegraphics[width=2.2in]{figs/Fig2c.png}
	\caption{\textbf{Illustration of the Friend-of-Friend approach on the
		\miami{} dataset.} (a) True daily incidence curve (left), (b) fitted
		daily incidence curve with logistic function (middle), and (c)
		distribution of lead time over 100 experiments (right). Note that this
		experiment does not reveal any lead time.}
		\label{fig_degreeMiamiSEIR}\vspace{-0.15in}
\end{figure*}

Figures~\ref{fig_degreeOregonSI} and~\ref{fig_degreeMiamiSEIR} depict the
results of experiments we did on two large contact networks---\oregon{} and
\miami{} (see Table~\ref{tab:dataset} for details)---using the \seir{} model. We
formed the sensor set using the approach given in~\cite{christakis:10:sensor}
and measured the \emph{average lead time} of the peaks for 100 runs (hence the
results are robust to stochastic fluctuations). For the \oregon{} dataset,
Fig.~\ref{fig_degreeOregonSI} shows that there is a 11 days lead time on average
for the peak in the sensor set with respect to the random set (see
Fig.~\ref{fig_degreeOregonSI}(c)). In contrast, for the \miami{} dataset, no
lead time for the sensor set is observed (see
Fig.~\ref{fig_degreeMiamiSEIR}(c)). 

There may be several possible reasons for these inconsistencies. First, the
'Friend-of-Friend' approach implicitly assumes that the lead time always
increases as we add more sensors into the set. Second, the lead time observation
is assumed to be independent of the underlying network topology structures,
which is clearly not the case. Finally, and most importantly, the work
in~\cite{christakis:10:sensor} does not formally define the problem it is trying
to solve, i.e., what objective does the sensor set optimize? 

In this paper, we study the same problem: forecasting the flu outbreak by
monitoring the social sensors. We present our formalisms and principled
solutions, which avoid the shortcomings of the Friend-of-Friend approach and
have several desirable properties. In particular, our contributions are:
\begin{enumerate}[itemsep=-1pt]
	\item We formally pose and study three variants of the sensor
		set selection problem.  
	\item We give an efficient heuristic based on the notion of graph dominators
		which solves one variant of the social sensor selection problems.
	\item We conduct extensive experiments on city-scale datasets based on
		detailed microscopic simulations, demonstrating improved lead time over
		competitors (including the Friend-of-Friend approach
		of~\cite{christakis:10:sensor}).
	\item We design surrogate/proxy social sensors using demographic information
		so that it is easy to deploy in practice without the knowledge of the
		full contact network.
\end{enumerate}
To the best of our knowledge, our work is the \emph{first} to systematically
formalize the problem of picking appropriate individuals to monitor and
forecast the disease spreading over a social contact network. The rest of this
paper is organized as follows. In Section~\ref{sec:background}, we introduce
some background knowledge about disease propagation models and social sensors
for disease outbreak monitoring and forecasting. The problem we intend to solve
in this paper is formulated in Section~\ref{sec:formulation}, followed by our
proposed solution in Section~\ref{sec:approach}. In Section~\ref{sec:exp}, we
show our experimental results on several large US city datasets. Finally, we
survey the related work in Section~\ref{sec:related}, and conclude this paper in
Section~\ref{sec:conclusion}.
